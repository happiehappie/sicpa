//
//  ArticleModels+Extensions.swift
//  NicolleTanTests
//
//  Created by Jack Xiong Lim on 24/7/21.
//

import Foundation
@testable import NicolleTan

extension ArticleModels.ArticleModelsViewModels {
    static func mock() -> Self {
        let value = MostPopularResponse.mock()
        var queriedArticles = [TitleSubtitleTableViewCellViewModel]()
        for article in value.results ?? [] {
            let vm = TitleSubtitleTableViewCellViewModel()
            vm.titleText.accept(article.title)
            if let date = DateFormatter.articleApiDateFormattor.date(from: article.published_date ?? "") {
                vm.subtitleText.accept(DateFormatter.articleDisplayDateFormattor.string(from: date))
            }
            queriedArticles.append(vm)
        }
        return ArticleModels.ArticleModelsViewModels(articles: queriedArticles)
    }
}
