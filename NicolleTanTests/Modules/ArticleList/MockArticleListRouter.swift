//
//  MockArticleListRouter.swift
//  NicolleTanTests
//
//  Created by Jack Xiong Lim on 24/7/21.
//

import Foundation
@testable import NicolleTan

class MockArticleListRouter: ArticleListRoutable {
    var handleError = false
}
