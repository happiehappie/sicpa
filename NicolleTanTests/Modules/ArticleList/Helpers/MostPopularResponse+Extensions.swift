//
//  MostPopularResponse+Extensions.swift
//  NicolleTanTests
//
//  Created by Jack Xiong Lim on 24/7/21.
//

import Foundation
import NicolleTan

extension MostPopularResponse {
    static func mock() -> Self {
        let decoder = JSONDecoder()
        let json = """
{
  "status": "OK",
  "copyright": "Copyright (c) 2021 The New York Times Company.  All Rights Reserved.",
  "num_results": 20,
  "results": [
    {
      "uri": "nyt://article/d3045f90-64e2-578b-b035-544dabdb2c42",
      "url": "https://www.nytimes.com/2021/07/21/well/move/weight-training-fat.html",
      "id": 100000007873785,
      "asset_id": 100000007873785,
      "source": "New York Times",
      "published_date": "2021-07-21",
      "updated": "2021-07-22 08:55:04",
      "section": "Well",
      "subsection": "Move",
      "nytdsection": "well",
      "adx_keywords": "Exercise;Muscles;Obesity;Mice;Research;Weight Lifting;Weight;FASEB Journal",
      "column": null,
      "byline": "By Gretchen Reynolds",
      "type": "Article",
      "title": "Lifting Weights? Your Fat Cells Would Like to Have a Word.",
      "abstract": "A cellular chat after your workout may explain in part why weight training burns fat.",
      "des_facet": [
        "Exercise",
        "Muscles",
        "Obesity",
        "Mice",
        "Research",
        "Weight Lifting",
        "Weight"
      ],
      "org_facet": [
        "FASEB Journal"
      ],
      "per_facet": [],
      "geo_facet": [],
      "media": [
        {
          "type": "image",
          "subtype": "photo",
          "caption": "",
          "copyright": "Andrew Testa for The New York Times",
          "approved_for_syndication": 1,
          "media-metadata": [
            {
              "url": "https://static01.nyt.com/images/2021/07/27/well/physed-weights/physed-weights-thumbStandard.jpg",
              "format": "Standard Thumbnail",
              "height": 75,
              "width": 75
            },
            {
              "url": "https://static01.nyt.com/images/2021/07/27/well/physed-weights/physed-weights-mediumThreeByTwo210-v2.jpg",
              "format": "mediumThreeByTwo210",
              "height": 140,
              "width": 210
            },
            {
              "url": "https://static01.nyt.com/images/2021/07/27/well/physed-weights/physed-weights-mediumThreeByTwo440-v2.jpg",
              "format": "mediumThreeByTwo440",
              "height": 293,
              "width": 440
            }
          ]
        }
      ],
      "eta_id": 0
    },
    {
      "uri": "nyt://article/7cd0e9bb-4400-5aa7-a4b0-281a85cdd6fb",
      "url": "https://www.nytimes.com/2021/07/21/us/mormon-women-underclothes.html",
      "id": 100000007856366,
      "asset_id": 100000007856366,
      "source": "New York Times",
      "published_date": "2021-07-21",
      "updated": "2021-07-22 00:56:42",
      "section": "U.S.",
      "subsection": "",
      "nytdsection": "u.s.",
      "adx_keywords": "Mormons (Church of Jesus Christ of Latter-Day Saints);Lingerie and Underwear;Women and Girls;Piton, Sasha",
      "column": null,
      "byline": "By Ruth Graham",
      "type": "Article",
      "title": "Among Mormon Women, Frank Talk About Sacred Underclothes",
      "abstract": "Frustrated by itchy, constrictive church-designed garments, they are asking for better fit, more options and “buttery soft fabric.”",
      "des_facet": [
        "Mormons (Church of Jesus Christ of Latter-Day Saints)",
        "Lingerie and Underwear",
        "Women and Girls"
      ],
      "org_facet": [],
      "per_facet": [
        "Piton, Sasha"
      ],
      "geo_facet": [],
      "media": [
        {
          "type": "image",
          "subtype": "photo",
          "caption": "Sasha Piton, 33, has called on the Church of Jesus Christ of Latter-day Saints to manufacture more breathable and comfortable temple garments for its members.",
          "copyright": "Kim Raff for The New York Times",
          "approved_for_syndication": 1,
          "media-metadata": [
            {
              "url": "https://static01.nyt.com/images/2021/07/13/us/00Mormon-garments/00Mormon-garments-thumbStandard.jpg",
              "format": "Standard Thumbnail",
              "height": 75,
              "width": 75
            },
            {
              "url": "https://static01.nyt.com/images/2021/07/13/us/00Mormon-garments/00Mormon-garments-mediumThreeByTwo210.jpg",
              "format": "mediumThreeByTwo210",
              "height": 140,
              "width": 210
            },
            {
              "url": "https://static01.nyt.com/images/2021/07/13/us/00Mormon-garments/00Mormon-garments-mediumThreeByTwo440.jpg",
              "format": "mediumThreeByTwo440",
              "height": 293,
              "width": 440
            }
          ]
        }
      ],
      "eta_id": 0
    },
    {
      "uri": "nyt://article/d01e1e18-baec-5585-abc6-494cade40d44",
      "url": "https://www.nytimes.com/2021/07/20/opinion/britain-freedom-day-johnson.html",
      "id": 100000007874096,
      "asset_id": 100000007874096,
      "source": "New York Times",
      "published_date": "2021-07-20",
      "updated": "2021-07-21 11:20:10",
      "section": "Opinion",
      "subsection": "",
      "nytdsection": "opinion",
      "adx_keywords": "Coronavirus (2019-nCoV);Disease Rates;Masks;Quarantines;Vaccination and Immunization;Politics and Government;Johnson, Boris;England;Great Britain",
      "column": null,
      "byline": "By Tanya Gold",
      "type": "Article",
      "title": "England Is ‘Free.’ And in Total Chaos.",
      "abstract": "We have no plan.",
      "des_facet": [
        "Coronavirus (2019-nCoV)",
        "Disease Rates",
        "Masks",
        "Quarantines",
        "Vaccination and Immunization",
        "Politics and Government"
      ],
      "org_facet": [],
      "per_facet": [
        "Johnson, Boris"
      ],
      "geo_facet": [
        "England",
        "Great Britain"
      ],
      "media": [
        {
          "type": "image",
          "subtype": "photo",
          "caption": "People on a dance floor in London shortly after the Freedom Day reopening on Monday.",
          "copyright": "Alberto Pezzali/Associated Press",
          "approved_for_syndication": 1,
          "media-metadata": [
            {
              "url": "https://static01.nyt.com/images/2021/07/20/opinion/20Gold1/20Gold1-thumbStandard.jpg",
              "format": "Standard Thumbnail",
              "height": 75,
              "width": 75
            },
            {
              "url": "https://static01.nyt.com/images/2021/07/20/opinion/20Gold1/20Gold1-mediumThreeByTwo210.jpg",
              "format": "mediumThreeByTwo210",
              "height": 140,
              "width": 210
            },
            {
              "url": "https://static01.nyt.com/images/2021/07/20/opinion/20Gold1/20Gold1-mediumThreeByTwo440.jpg",
              "format": "mediumThreeByTwo440",
              "height": 293,
              "width": 440
            }
          ]
        }
      ],
      "eta_id": 0
    },
    {
      "uri": "nyt://article/fdea7cd5-fb69-5ec2-863c-5e55412f4b19",
      "url": "https://www.nytimes.com/2021/07/20/world/europe/england-canal-boats-permanent-homes.html",
      "id": 100000007854258,
      "asset_id": 100000007854258,
      "source": "New York Times",
      "published_date": "2021-07-20",
      "updated": "2021-07-21 13:24:52",
      "section": "World",
      "subsection": "Europe",
      "nytdsection": "world",
      "adx_keywords": "Canals;Houseboats;Real Estate and Housing (Residential);Quarantine (Life and Culture);England;Great Britain",
      "column": null,
      "byline": "By Megan Specia",
      "type": "Article",
      "title": "On England’s Canals, Boaters Embrace the Peace and Pace of a Floating Life",
      "abstract": "More people are calling England’s canals — and the narrow boats used to navigate them — home as remote work options in the pandemic’s wake make a mobile lifestyle more possible.",
      "des_facet": [
        "Canals",
        "Houseboats",
        "Real Estate and Housing (Residential)",
        "Quarantine (Life and Culture)"
      ],
      "org_facet": [],
      "per_facet": [],
      "geo_facet": [
        "England",
        "Great Britain"
      ],
      "media": [
        {
          "type": "image",
          "subtype": "photo",
          "caption": "Canal boats on the Grand Union Canal near Daventry, England.",
          "copyright": "Andrew Testa for The New York Times",
          "approved_for_syndication": 1,
          "media-metadata": [
            {
              "url": "https://static01.nyt.com/images/2021/07/15/world/00uk-canal-dispatch-01/00uk-canal-dispatch-01-thumbStandard.jpg",
              "format": "Standard Thumbnail",
              "height": 75,
              "width": 75
            },
            {
              "url": "https://static01.nyt.com/images/2021/07/15/world/00uk-canal-dispatch-01/00uk-canal-dispatch-01-mediumThreeByTwo210-v2.jpg",
              "format": "mediumThreeByTwo210",
              "height": 140,
              "width": 210
            },
            {
              "url": "https://static01.nyt.com/images/2021/07/15/world/00uk-canal-dispatch-01/00uk-canal-dispatch-01-mediumThreeByTwo440-v2.jpg",
              "format": "mediumThreeByTwo440",
              "height": 293,
              "width": 440
            }
          ]
        }
      ],
      "eta_id": 0
    },
    {
      "uri": "nyt://article/939063fa-cd2e-5837-9795-2acf3a796a57",
      "url": "https://www.nytimes.com/2021/07/21/opinion/young-adults-spatial-reasoning.html",
      "id": 100000007872779,
      "asset_id": 100000007872779,
      "source": "New York Times",
      "published_date": "2021-07-21",
      "updated": "2021-07-21 12:56:15",
      "section": "Opinion",
      "subsection": "",
      "nytdsection": "opinion",
      "adx_keywords": "Tests and Examinations;Black People;Education (K-12);For-Profit Schools;Colleges and Universities;Admissions Standards;Minorities;Reading and Writing Skills (Education);Income Inequality;Race and Ethnicity;United States Economy;Educational Testing Service",
      "column": null,
      "byline": "By Thomas B. Edsall",
      "type": "Article",
      "title": "We Are Leaving ‘Lost Einsteins’ Behind",
      "abstract": "Hundreds of thousands of highly capable people are being dropped by the wayside.",
      "des_facet": [
        "Tests and Examinations",
        "Black People",
        "Education (K-12)",
        "For-Profit Schools",
        "Colleges and Universities",
        "Admissions Standards",
        "Minorities",
        "Reading and Writing Skills (Education)",
        "Income Inequality",
        "Race and Ethnicity",
        "United States Economy"
      ],
      "org_facet": [
        "Educational Testing Service"
      ],
      "per_facet": [],
      "geo_facet": [],
      "media": [
        {
          "type": "image",
          "subtype": "photo",
          "caption": "",
          "copyright": "Mark Makela for The New York Times",
          "approved_for_syndication": 1,
          "media-metadata": [
            {
              "url": "https://static01.nyt.com/images/2021/07/21/opinion/21edsall-lead/21edsall-lead-thumbStandard.jpg",
              "format": "Standard Thumbnail",
              "height": 75,
              "width": 75
            },
            {
              "url": "https://static01.nyt.com/images/2021/07/21/opinion/21edsall-lead/21edsall-lead-mediumThreeByTwo210-v2.jpg",
              "format": "mediumThreeByTwo210",
              "height": 140,
              "width": 210
            },
            {
              "url": "https://static01.nyt.com/images/2021/07/21/opinion/21edsall-lead/21edsall-lead-mediumThreeByTwo440-v2.jpg",
              "format": "mediumThreeByTwo440",
              "height": 293,
              "width": 440
            }
          ]
        }
      ],
      "eta_id": 0
    },
    {
      "uri": "nyt://article/8bd169c5-9761-5165-be2f-77142697f884",
      "url": "https://www.nytimes.com/2021/07/20/health/coronavirus-johnson-vaccine-delta.html",
      "id": 100000007875351,
      "asset_id": 100000007875351,
      "source": "New York Times",
      "published_date": "2021-07-20",
      "updated": "2021-07-21 14:21:01",
      "section": "Health",
      "subsection": "",
      "nytdsection": "health",
      "adx_keywords": "your-feed-science;Vaccination and Immunization;Blood Clots;Disease Rates;Coronavirus (2019-nCoV);Research;Drugs (Pharmaceuticals);Immune System;AstraZeneca PLC;Food and Drug Administration;Johnson & Johnson;Moderna Inc;Pfizer Inc",
      "column": null,
      "byline": "By Apoorva Mandavilli",
      "type": "Article",
      "title": "J.&J. Vaccine May Be Less Effective Against Delta, Study Suggests",
      "abstract": "Many who received the shot may need to consider boosters, the authors said. But federal health officials do not recommend second doses.",
      "des_facet": [
        "your-feed-science",
        "Vaccination and Immunization",
        "Blood Clots",
        "Disease Rates",
        "Coronavirus (2019-nCoV)",
        "Research",
        "Drugs (Pharmaceuticals)",
        "Immune System"
      ],
      "org_facet": [
        "AstraZeneca PLC",
        "Food and Drug Administration",
        "Johnson & Johnson",
        "Moderna Inc",
        "Pfizer Inc"
      ],
      "per_facet": [],
      "geo_facet": [],
      "media": [
        {
          "type": "image",
          "subtype": "photo",
          "caption": "Receiving the Johnson &amp; Johnson vaccine in Philadelphia in May. The study suggests that 13 million people inoculated with the vaccine may need a second dose.",
          "copyright": "Rachel Wisniewski for The New York Times",
          "approved_for_syndication": 1,
          "media-metadata": [
            {
              "url": "https://static01.nyt.com/images/2021/07/20/science/20virus-JnJ1/merlin_187566546_ad23a53f-b2e4-41dc-a838-45a745fc46a6-thumbStandard.jpg",
              "format": "Standard Thumbnail",
              "height": 75,
              "width": 75
            },
            {
              "url": "https://static01.nyt.com/images/2021/07/20/science/20virus-JnJ1/20virus-JnJ1-mediumThreeByTwo210.jpg",
              "format": "mediumThreeByTwo210",
              "height": 140,
              "width": 210
            },
            {
              "url": "https://static01.nyt.com/images/2021/07/20/science/20virus-JnJ1/20virus-JnJ1-mediumThreeByTwo440.jpg",
              "format": "mediumThreeByTwo440",
              "height": 293,
              "width": 440
            }
          ]
        }
      ],
      "eta_id": 0
    },
    {
      "uri": "nyt://article/2385755c-674c-582d-8bad-80ff8cad8689",
      "url": "https://www.nytimes.com/2021/07/21/magazine/americans-with-disabilities-act.html",
      "id": 100000007871627,
      "asset_id": 100000007871627,
      "source": "New York Times",
      "published_date": "2021-07-21",
      "updated": "2021-07-21 17:53:19",
      "section": "Magazine",
      "subsection": "",
      "nytdsection": "magazine",
      "adx_keywords": "Suits and Litigation (Civil);Disabilities;Restaurants;Law and Legislation;California",
      "column": null,
      "byline": "By Lauren Markham",
      "type": "Article",
      "title": "The Man Who Filed More Than 180 Disability Lawsuits",
      "abstract": "Is it profiteering — or justice?",
      "des_facet": [
        "Suits and Litigation (Civil)",
        "Disabilities",
        "Restaurants",
        "Law and Legislation"
      ],
      "org_facet": [],
      "per_facet": [],
      "geo_facet": [
        "California"
      ],
      "media": [
        {
          "type": "image",
          "subtype": "photo",
          "caption": "Albert Dytch uses a wheelchair because of muscular dystrophy.",
          "copyright": "Balazs Gardi for The New York Times",
          "approved_for_syndication": 1,
          "media-metadata": [
            {
              "url": "https://static01.nyt.com/images/2021/07/24/magazine/25mag-ADA-image/24mag-ADA-image-thumbStandard.jpg",
              "format": "Standard Thumbnail",
              "height": 75,
              "width": 75
            },
            {
              "url": "https://static01.nyt.com/images/2021/07/24/magazine/25mag-ADA-image/24mag-ADA-image-mediumThreeByTwo210.jpg",
              "format": "mediumThreeByTwo210",
              "height": 140,
              "width": 210
            },
            {
              "url": "https://static01.nyt.com/images/2021/07/24/magazine/25mag-ADA-image/24mag-ADA-image-mediumThreeByTwo440.jpg",
              "format": "mediumThreeByTwo440",
              "height": 293,
              "width": 440
            }
          ]
        }
      ],
      "eta_id": 0
    },
    {
      "uri": "nyt://article/329eabe9-ea21-5b30-92f6-31711b6f5c36",
      "url": "https://www.nytimes.com/2021/07/19/opinion/republicans-donald-trump-misinformation.html",
      "id": 100000007873755,
      "asset_id": 100000007873755,
      "source": "New York Times",
      "published_date": "2021-07-19",
      "updated": "2021-07-20 00:51:19",
      "section": "Opinion",
      "subsection": "",
      "nytdsection": "opinion",
      "adx_keywords": "Conservatism (US Politics);Vaccination and Immunization;United States Politics and Government;Trump, Donald J;Republican Party",
      "column": null,
      "byline": "By Paul Krugman",
      "type": "Article",
      "title": "Republicans Have Their Own Private Autocracy",
      "abstract": "Loyalty signaling, flattery inflation and the modern G.O.P.",
      "des_facet": [
        "Conservatism (US Politics)",
        "Vaccination and Immunization",
        "United States Politics and Government"
      ],
      "org_facet": [
        "Republican Party"
      ],
      "per_facet": [
        "Trump, Donald J"
      ],
      "geo_facet": [],
      "media": [
        {
          "type": "image",
          "subtype": "photo",
          "caption": "",
          "copyright": "Gabriella Demczuk for The New York Times",
          "approved_for_syndication": 1,
          "media-metadata": [
            {
              "url": "https://static01.nyt.com/images/2021/07/19/opinion/19krugman-final/19krugman-final-thumbStandard.jpg",
              "format": "Standard Thumbnail",
              "height": 75,
              "width": 75
            },
            {
              "url": "https://static01.nyt.com/images/2021/07/19/opinion/19krugman-final/19krugman-final-mediumThreeByTwo210.jpg",
              "format": "mediumThreeByTwo210",
              "height": 140,
              "width": 210
            },
            {
              "url": "https://static01.nyt.com/images/2021/07/19/opinion/19krugman-final/19krugman-final-mediumThreeByTwo440.jpg",
              "format": "mediumThreeByTwo440",
              "height": 293,
              "width": 440
            }
          ]
        }
      ],
      "eta_id": 0
    },
    {
      "uri": "nyt://article/6ae4d4e6-2cbd-5cba-b9fc-9a731141ba6f",
      "url": "https://www.nytimes.com/2021/07/20/us/utah-water-drought-climate-change.html",
      "id": 100000007863950,
      "asset_id": 100000007863950,
      "source": "New York Times",
      "published_date": "2021-07-20",
      "updated": "2021-07-22 05:59:08",
      "section": "U.S.",
      "subsection": "",
      "nytdsection": "u.s.",
      "adx_keywords": "Drought;Water;Conservation of Resources;Agriculture and Farming;Real Estate and Housing (Residential);Building (Construction);Utah;Oakley (Utah)",
      "column": null,
      "byline": "By Jack Healy and Sophie Kasakove",
      "type": "Article",
      "title": "A Drought So Dire That a Utah Town Pulled the Plug on Growth",
      "abstract": "Groundwater and streams vital to both farmers and cities are drying up in the West, challenging the future of development.",
      "des_facet": [
        "Drought",
        "Water",
        "Conservation of Resources",
        "Agriculture and Farming",
        "Real Estate and Housing (Residential)",
        "Building (Construction)"
      ],
      "org_facet": [],
      "per_facet": [],
      "geo_facet": [
        "Utah",
        "Oakley (Utah)"
      ],
      "media": [
        {
          "type": "image",
          "subtype": "photo",
          "caption": "Some of the last homes currently being built in Oakley, Utah. The town has cut off new development because it doesn’t have enough water to go around.",
          "copyright": "Lindsay D’Addato for The New York Times",
          "approved_for_syndication": 1,
          "media-metadata": [
            {
              "url": "https://static01.nyt.com/images/2021/07/16/us/00utah-1/00utah-1-thumbStandard.jpg",
              "format": "Standard Thumbnail",
              "height": 75,
              "width": 75
            },
            {
              "url": "https://static01.nyt.com/images/2021/07/16/us/00utah-1/00utah-1-mediumThreeByTwo210-v2.jpg",
              "format": "mediumThreeByTwo210",
              "height": 140,
              "width": 210
            },
            {
              "url": "https://static01.nyt.com/images/2021/07/16/us/00utah-1/00utah-1-mediumThreeByTwo440-v2.jpg",
              "format": "mediumThreeByTwo440",
              "height": 293,
              "width": 440
            }
          ]
        }
      ],
      "eta_id": 0
    },
    {
      "uri": "nyt://article/355ac00b-442c-5ebf-947f-b564b2b61c9a",
      "url": "https://www.nytimes.com/2021/07/20/opinion/covid-return-to-office.html",
      "id": 100000007872775,
      "asset_id": 100000007872775,
      "source": "New York Times",
      "published_date": "2021-07-20",
      "updated": "2021-07-21 14:20:34",
      "section": "Opinion",
      "subsection": "",
      "nytdsection": "opinion",
      "adx_keywords": "Work-Life Balance;Working Hours;Telecommuting;Labor and Jobs;Wages and Salaries;Productivity;Quarantine (Life and Culture);United States",
      "column": null,
      "byline": "By Bryce Covert",
      "type": "Article",
      "title": "8 Hours a Day, 5 Days a Week Is Not Working for Us",
      "abstract": "The goal is “one reasonable job per person,” not “two for one and half for another.”",
      "des_facet": [
        "Work-Life Balance",
        "Working Hours",
        "Telecommuting",
        "Labor and Jobs",
        "Wages and Salaries",
        "Productivity",
        "Quarantine (Life and Culture)"
      ],
      "org_facet": [],
      "per_facet": [],
      "geo_facet": [
        "United States"
      ],
      "media": [
        {
          "type": "image",
          "subtype": "photo",
          "caption": "",
          "copyright": "Illustration by The New York Times; photograph by Heath Korvola/Getty Images",
          "approved_for_syndication": 1,
          "media-metadata": [
            {
              "url": "https://static01.nyt.com/images/2021/07/20/opinion/20covert_illo/20covert_illo-thumbStandard.jpg",
              "format": "Standard Thumbnail",
              "height": 75,
              "width": 75
            },
            {
              "url": "https://static01.nyt.com/images/2021/07/20/opinion/20covert_illo/20covert_illo-mediumThreeByTwo210.jpg",
              "format": "mediumThreeByTwo210",
              "height": 140,
              "width": 210
            },
            {
              "url": "https://static01.nyt.com/images/2021/07/20/opinion/20covert_illo/20covert_illo-mediumThreeByTwo440.jpg",
              "format": "mediumThreeByTwo440",
              "height": 293,
              "width": 440
            }
          ]
        }
      ],
      "eta_id": 0
    },
    {
      "uri": "nyt://article/0f067ea8-f90d-5908-a60a-790e1259f616",
      "url": "https://www.nytimes.com/2021/07/20/opinion/eric-adams-nyc-mayor.html",
      "id": 100000007875367,
      "asset_id": 100000007875367,
      "source": "New York Times",
      "published_date": "2021-07-20",
      "updated": "2021-07-22 05:53:32",
      "section": "Opinion",
      "subsection": "",
      "nytdsection": "opinion",
      "adx_keywords": "Elections, Mayors;Content Type: Personal Profile;Mayors;Police Reform;Police Brutality, Misconduct and Shootings;Police;Racial Profiling;Black People;United States Politics and Government;Adams, Eric L;Democratic Party;Police Department (NYC);New York City",
      "column": null,
      "byline": "By Bret Stephens",
      "type": "Article",
      "title": "Eric Adams Is Going to Save New York",
      "abstract": "For progressives, he's a nightmare. For the rest of big-city America,  he’s a godsend.",
      "des_facet": [
        "Elections, Mayors",
        "Content Type: Personal Profile",
        "Mayors",
        "Police Reform",
        "Police Brutality, Misconduct and Shootings",
        "Police",
        "Racial Profiling",
        "Black People",
        "United States Politics and Government"
      ],
      "org_facet": [
        "Democratic Party",
        "Police Department (NYC)"
      ],
      "per_facet": [
        "Adams, Eric L"
      ],
      "geo_facet": [
        "New York City"
      ],
      "media": [
        {
          "type": "image",
          "subtype": "photo",
          "caption": "",
          "copyright": "Damon Winter/The New York Times",
          "approved_for_syndication": 1,
          "media-metadata": [
            {
              "url": "https://static01.nyt.com/images/2021/07/20/opinion/20stephens-lead/20stephens-lead-thumbStandard.jpg",
              "format": "Standard Thumbnail",
              "height": 75,
              "width": 75
            },
            {
              "url": "https://static01.nyt.com/images/2021/07/20/opinion/20stephens-lead/20stephens-lead-mediumThreeByTwo210.jpg",
              "format": "mediumThreeByTwo210",
              "height": 140,
              "width": 210
            },
            {
              "url": "https://static01.nyt.com/images/2021/07/20/opinion/20stephens-lead/20stephens-lead-mediumThreeByTwo440.jpg",
              "format": "mediumThreeByTwo440",
              "height": 293,
              "width": 440
            }
          ]
        }
      ],
      "eta_id": 0
    },
    {
      "uri": "nyt://article/32e79528-e235-55f6-bd6c-114cc966f46a",
      "url": "https://www.nytimes.com/2021/07/20/science/suburbs-wildlife-deer-owls.html",
      "id": 100000007871209,
      "asset_id": 100000007871209,
      "source": "New York Times",
      "published_date": "2021-07-20",
      "updated": "2021-07-20 21:00:33",
      "section": "Science",
      "subsection": "",
      "nytdsection": "science",
      "adx_keywords": "Animals;Deer;Birds;your-feed-science;your-feed-animals;Suburbs;Animal Nation Inc;Westchester County (NY)",
      "column": null,
      "byline": "By Sabrina Imbler and Linda Kuo",
      "type": "Article",
      "title": "Caring for the Wildlife That Stray Into the Suburbs",
      "abstract": "It’s baby-animal season in the towns north of New York. But with many of the mothers gone missing, humans are stepping in to help out.",
      "des_facet": [
        "Animals",
        "Deer",
        "Birds",
        "your-feed-science",
        "your-feed-animals",
        "Suburbs"
      ],
      "org_facet": [
        "Animal Nation Inc"
      ],
      "per_facet": [],
      "geo_facet": [
        "Westchester County (NY)"
      ],
      "media": [
        {
          "type": "image",
          "subtype": "photo",
          "caption": "Mr. Moore of Animal Nation hand-feeding fawns. Some were orphaned when their mothers were killed by cars or hunters; others were brought in by people who mistakenly thought they had been abandoned.",
          "copyright": "Linda Kuo",
          "approved_for_syndication": 0,
          "media-metadata": [
            {
              "url": "https://static01.nyt.com/images/2021/07/20/science/20SCI-SUBANIMALS-promo/20SCI-SUBANIMALS-promo-thumbStandard.jpg",
              "format": "Standard Thumbnail",
              "height": 75,
              "width": 75
            },
            {
              "url": "https://static01.nyt.com/images/2021/07/20/science/20SCI-SUBANIMALS-promo/20SCI-SUBANIMALS-promo-mediumThreeByTwo210.jpg",
              "format": "mediumThreeByTwo210",
              "height": 140,
              "width": 210
            },
            {
              "url": "https://static01.nyt.com/images/2021/07/20/science/20SCI-SUBANIMALS-promo/20SCI-SUBANIMALS-promo-mediumThreeByTwo440.jpg",
              "format": "mediumThreeByTwo440",
              "height": 293,
              "width": 440
            }
          ]
        }
      ],
      "eta_id": 0
    },
    {
      "uri": "nyt://article/2306997a-0e48-555e-8e9e-d73cd9e3270c",
      "url": "https://www.nytimes.com/2021/07/21/us/american-life-expectancy-report.html",
      "id": 100000007877192,
      "asset_id": 100000007877192,
      "source": "New York Times",
      "published_date": "2021-07-21",
      "updated": "2021-07-21 22:45:08",
      "section": "U.S.",
      "subsection": "",
      "nytdsection": "u.s.",
      "adx_keywords": "Coronavirus (2019-nCoV);Longevity;Hispanic-Americans;Whites;Black People;Race and Ethnicity;United States",
      "column": null,
      "byline": "By Julie Bosman, Sophie Kasakove and Daniel Victor",
      "type": "Article",
      "title": "U.S. Life Expectancy Plunged in 2020, Especially for Black and Hispanic Americans",
      "abstract": "The 18-month drop, the steepest decline since World War II, was fueled by the coronavirus pandemic.",
      "des_facet": [
        "Coronavirus (2019-nCoV)",
        "Longevity",
        "Hispanic-Americans",
        "Whites",
        "Black People",
        "Race and Ethnicity"
      ],
      "org_facet": [],
      "per_facet": [],
      "geo_facet": [
        "United States"
      ],
      "media": [
        {
          "type": "image",
          "subtype": "photo",
          "caption": "A coronavirus testing site in San Bernardino, Calif., in December. The difficult year deepened racial and ethnic disparities in life expectancy, with Black and Hispanic Americans losing nearly two more years than white Americans.",
          "copyright": "Alex Welsh for The New York Times",
          "approved_for_syndication": 1,
          "media-metadata": [
            {
              "url": "https://static01.nyt.com/images/2021/07/21/multimedia/21xp-life/21xp-life-thumbStandard.jpg",
              "format": "Standard Thumbnail",
              "height": 75,
              "width": 75
            },
            {
              "url": "https://static01.nyt.com/images/2021/07/21/multimedia/21xp-life/21xp-life-mediumThreeByTwo210.jpg",
              "format": "mediumThreeByTwo210",
              "height": 140,
              "width": 210
            },
            {
              "url": "https://static01.nyt.com/images/2021/07/21/multimedia/21xp-life/21xp-life-mediumThreeByTwo440.jpg",
              "format": "mediumThreeByTwo440",
              "height": 293,
              "width": 440
            }
          ]
        }
      ],
      "eta_id": 0
    },
    {
      "uri": "nyt://article/e7ddaabf-82b8-5a93-a08e-7835cfc6a7ab",
      "url": "https://www.nytimes.com/2021/07/20/dining/california-barbecue-restaurants.html",
      "id": 100000007868993,
      "asset_id": 100000007868993,
      "source": "New York Times",
      "published_date": "2021-07-20",
      "updated": "2021-07-21 16:18:19",
      "section": "Food",
      "subsection": "",
      "nytdsection": "food",
      "adx_keywords": "Cooking and Cookbooks;Grilling (Cooking);Barbecue;Meat;Restaurants;California",
      "column": null,
      "byline": "By Tejal Rao",
      "type": "Article",
      "title": "In Search of the California Barbecue Tradition",
      "abstract": "From storied Santa Maria tri-tip on the Central Coast, to barbacoa in Los Angeles, to hot links in West Oakland, the rules for slow-and-low are constantly rewritten here.",
      "des_facet": [
        "Cooking and Cookbooks",
        "Grilling (Cooking)",
        "Barbecue",
        "Meat",
        "Restaurants"
      ],
      "org_facet": [],
      "per_facet": [],
      "geo_facet": [
        "California"
      ],
      "media": [
        {
          "type": "image",
          "subtype": "photo",
          "caption": "In addition to smoked turkey, chicken, brisket and ribs, Mr. Horn makes hot links, and cooks whole hog barbecue on Sundays.",
          "copyright": "Adam Amengual for The New York Times",
          "approved_for_syndication": 1,
          "media-metadata": [
            {
              "url": "https://static01.nyt.com/images/2021/07/21/dining/20calbbq11/20calbbq11-thumbStandard.jpg",
              "format": "Standard Thumbnail",
              "height": 75,
              "width": 75
            },
            {
              "url": "https://static01.nyt.com/images/2021/07/21/dining/20calbbq11/20calbbq11-mediumThreeByTwo210.jpg",
              "format": "mediumThreeByTwo210",
              "height": 140,
              "width": 210
            },
            {
              "url": "https://static01.nyt.com/images/2021/07/21/dining/20calbbq11/20calbbq11-mediumThreeByTwo440.jpg",
              "format": "mediumThreeByTwo440",
              "height": 293,
              "width": 440
            }
          ]
        }
      ],
      "eta_id": 0
    },
    {
      "uri": "nyt://article/77322d7a-a97e-5a45-8dda-c3dd9d32ec5a",
      "url": "https://www.nytimes.com/2020/07/02/parenting/intrusive-thoughts-scary-images-child-dying.html",
      "id": 100000007200943,
      "asset_id": 100000007200943,
      "source": "New York Times",
      "published_date": "2020-07-02",
      "updated": "2020-07-05 16:42:07",
      "section": "Parenting",
      "subsection": "",
      "nytdsection": "parenting",
      "adx_keywords": "Anxiety and Stress;Parenting;Children and Childhood;Fear (Emotion);Drownings;Mental Health and Disorders",
      "column": null,
      "byline": "By Juli Fraga and Karen Kleiman",
      "type": "Article",
      "title": "Having Disturbing Thoughts as a New Parent? Here’s How to Cope",
      "abstract": "Intrusive thoughts can be terrifying. Exercises, like distancing, can reduce parents’ anxieties.",
      "des_facet": [
        "Anxiety and Stress",
        "Parenting",
        "Children and Childhood",
        "Fear (Emotion)",
        "Drownings",
        "Mental Health and Disorders"
      ],
      "org_facet": [],
      "per_facet": [],
      "geo_facet": [],
      "media": [
        {
          "type": "image",
          "subtype": "photo",
          "caption": "",
          "copyright": "Helen Li",
          "approved_for_syndication": 0,
          "media-metadata": [
            {
              "url": "https://static01.nyt.com/images/2020/06/26/multimedia/26parenting-intrusive/26parenting-intrusive-thumbStandard-v2.jpg",
              "format": "Standard Thumbnail",
              "height": 75,
              "width": 75
            },
            {
              "url": "https://static01.nyt.com/images/2020/06/26/multimedia/26parenting-intrusive/26parenting-intrusive-mediumThreeByTwo210-v2.jpg",
              "format": "mediumThreeByTwo210",
              "height": 140,
              "width": 210
            },
            {
              "url": "https://static01.nyt.com/images/2020/06/26/multimedia/26parenting-intrusive/26parenting-intrusive-mediumThreeByTwo440-v2.jpg",
              "format": "mediumThreeByTwo440",
              "height": 293,
              "width": 440
            }
          ]
        }
      ],
      "eta_id": 0
    },
    {
      "uri": "nyt://article/354be4b0-b10d-5c3c-a3ac-8e55eda02f31",
      "url": "https://www.nytimes.com/2021/07/19/climate/bootleg-wildfire-weather.html",
      "id": 100000007873885,
      "asset_id": 100000007873885,
      "source": "New York Times",
      "published_date": "2021-07-19",
      "updated": "2021-07-22 10:19:05",
      "section": "Climate",
      "subsection": "",
      "nytdsection": "climate",
      "adx_keywords": "Global Warming;Wildfires;Weather;Forests and Forestry;Fires and Firefighters;Lightning;California;Oregon",
      "column": null,
      "byline": "By Henry Fountain",
      "type": "Article",
      "title": "How Bad Is the Bootleg Fire? It’s Generating Its Own Weather.",
      "abstract": "Unpredictable winds, fire clouds that spawn lightning, and flames that leap over firebreaks are confounding efforts to fight the blaze, which is sweeping through southern Oregon.",
      "des_facet": [
        "Global Warming",
        "Wildfires",
        "Weather",
        "Forests and Forestry",
        "Fires and Firefighters",
        "Lightning"
      ],
      "org_facet": [],
      "per_facet": [],
      "geo_facet": [
        "California",
        "Oregon"
      ],
      "media": [
        {
          "type": "image",
          "subtype": "photo",
          "caption": "",
          "copyright": "Mathieu Lewis-Rolland/Reuters",
          "approved_for_syndication": 1,
          "media-metadata": [
            {
              "url": "https://static01.nyt.com/images/2021/07/19/climate/1CLI-FIREWEATHER-promo/1CLI-FIREWEATHER-promo-thumbStandard.jpg",
              "format": "Standard Thumbnail",
              "height": 75,
              "width": 75
            },
            {
              "url": "https://static01.nyt.com/images/2021/07/19/climate/1CLI-FIREWEATHER-promo/merlin_190918275_9a7d0858-e4a2-47bf-add3-21e426dbb116-mediumThreeByTwo210.jpg",
              "format": "mediumThreeByTwo210",
              "height": 140,
              "width": 210
            },
            {
              "url": "https://static01.nyt.com/images/2021/07/19/climate/1CLI-FIREWEATHER-promo/merlin_190918275_9a7d0858-e4a2-47bf-add3-21e426dbb116-mediumThreeByTwo440.jpg",
              "format": "mediumThreeByTwo440",
              "height": 293,
              "width": 440
            }
          ]
        }
      ],
      "eta_id": 0
    },
    {
      "uri": "nyt://article/27818e50-836c-5bf6-a487-d8421a93a6f0",
      "url": "https://www.nytimes.com/2021/07/16/dining/no-bake-desserts-recipes.html",
      "id": 100000007862946,
      "asset_id": 100000007862946,
      "source": "New York Times",
      "published_date": "2021-07-16",
      "updated": "2021-07-16 12:31:03",
      "section": "Food",
      "subsection": "",
      "nytdsection": "food",
      "adx_keywords": "Cooking and Cookbooks;Summer (Season);Desserts",
      "column": null,
      "byline": "By Margaux Laskey",
      "type": "Article",
      "title": "16 No-Bake Desserts for Blazing Summer Days",
      "abstract": "When it’s too hot to turn on the oven, these treats will satisfy your sweet tooth.",
      "des_facet": [
        "Cooking and Cookbooks",
        "Summer (Season)",
        "Desserts"
      ],
      "org_facet": [],
      "per_facet": [],
      "geo_facet": [],
      "media": [
        {
          "type": "image",
          "subtype": "photo",
          "caption": "",
          "copyright": "Christopher Testani for The New York Times.",
          "approved_for_syndication": 1,
          "media-metadata": [
            {
              "url": "https://static01.nyt.com/images/2019/07/22/dining/16no-bake-roundup-spumoni-ice-cream-cake/merlin_158023122_3d451d2c-295a-4dfb-ab52-4e9bd158225f-thumbStandard.jpg",
              "format": "Standard Thumbnail",
              "height": 75,
              "width": 75
            },
            {
              "url": "https://static01.nyt.com/images/2019/07/22/dining/16no-bake-roundup-spumoni-ice-cream-cake/merlin_158023122_3d451d2c-295a-4dfb-ab52-4e9bd158225f-mediumThreeByTwo210.jpg",
              "format": "mediumThreeByTwo210",
              "height": 140,
              "width": 210
            },
            {
              "url": "https://static01.nyt.com/images/2019/07/22/dining/16no-bake-roundup-spumoni-ice-cream-cake/merlin_158023122_3d451d2c-295a-4dfb-ab52-4e9bd158225f-mediumThreeByTwo440.jpg",
              "format": "mediumThreeByTwo440",
              "height": 293,
              "width": 440
            }
          ]
        }
      ],
      "eta_id": 0
    },
    {
      "uri": "nyt://article/0da5ecec-f81d-5dcd-a98e-d0356e96309b",
      "url": "https://www.nytimes.com/2021/07/19/opinion/trump-covid-extremism-loneliness.html",
      "id": 100000007873922,
      "asset_id": 100000007873922,
      "source": "New York Times",
      "published_date": "2021-07-19",
      "updated": "2021-07-20 18:38:34",
      "section": "Opinion",
      "subsection": "",
      "nytdsection": "opinion",
      "adx_keywords": "Loneliness;United States Politics and Government;Right-Wing Extremism and Alt-Right;QAnon;Coronavirus (2019-nCoV);Social Conditions and Trends;Trump, Donald J;Bender, Michael C;Rothschild, Mike (Journalist);United States",
      "column": null,
      "byline": "By Michelle Goldberg",
      "type": "Article",
      "title": "Loneliness Is Breaking America",
      "abstract": "Authoritarianism attracts isolated people.",
      "des_facet": [
        "Loneliness",
        "United States Politics and Government",
        "Right-Wing Extremism and Alt-Right",
        "QAnon",
        "Coronavirus (2019-nCoV)",
        "Social Conditions and Trends"
      ],
      "org_facet": [],
      "per_facet": [
        "Trump, Donald J",
        "Bender, Michael C",
        "Rothschild, Mike (Journalist)"
      ],
      "geo_facet": [
        "United States"
      ],
      "media": [
        {
          "type": "image",
          "subtype": "photo",
          "caption": "",
          "copyright": "Damon Winter/The New York Times",
          "approved_for_syndication": 1,
          "media-metadata": [
            {
              "url": "https://static01.nyt.com/images/2021/07/19/opinion/19goldberg-second/19goldberg-second-thumbStandard-v2.jpg",
              "format": "Standard Thumbnail",
              "height": 75,
              "width": 75
            },
            {
              "url": "https://static01.nyt.com/images/2021/07/19/opinion/19goldberg-second/19goldberg-second-mediumThreeByTwo210-v3.jpg",
              "format": "mediumThreeByTwo210",
              "height": 140,
              "width": 210
            },
            {
              "url": "https://static01.nyt.com/images/2021/07/19/opinion/19goldberg-second/19goldberg-second-mediumThreeByTwo440-v3.jpg",
              "format": "mediumThreeByTwo440",
              "height": 293,
              "width": 440
            }
          ]
        }
      ],
      "eta_id": 0
    },
    {
      "uri": "nyt://article/fb9d5e6a-aefb-53b1-b4ab-4c4b7a77dc18",
      "url": "https://www.nytimes.com/2021/07/21/opinion/haiti-us-history.html",
      "id": 100000007875893,
      "asset_id": 100000007875893,
      "source": "New York Times",
      "published_date": "2021-07-21",
      "updated": "2021-07-22 10:33:45",
      "section": "Opinion",
      "subsection": "",
      "nytdsection": "opinion",
      "adx_keywords": "Slavery (Historical);Black People;Colonization;Haiti;United States",
      "column": null,
      "byline": "By Annette Gordon-Reed",
      "type": "Article",
      "title": "We Owe Haiti a Debt We Can’t Repay",
      "abstract": "Haitians carried out the first and only successful slave revolt in modern history, then repelled Napoleon's forces, making way for the Louisiana Purchase. ",
      "des_facet": [
        "Slavery (Historical)",
        "Black People",
        "Colonization"
      ],
      "org_facet": [],
      "per_facet": [],
      "geo_facet": [
        "Haiti",
        "United States"
      ],
      "media": [
        {
          "type": "image",
          "subtype": "photo",
          "caption": "In 1791 the enslaved people of Haiti, then known as Saint-Domingue, engineered the first and only successful slave revolt in modern history. ",
          "copyright": "API/Gamma-Rapho, via Getty Images",
          "approved_for_syndication": 1,
          "media-metadata": [
            {
              "url": "https://static01.nyt.com/images/2021/07/21/opinion/21Gordon-Reed/21Gordon-Reed-thumbStandard.jpg",
              "format": "Standard Thumbnail",
              "height": 75,
              "width": 75
            },
            {
              "url": "https://static01.nyt.com/images/2021/07/21/opinion/21Gordon-Reed/21Gordon-Reed-mediumThreeByTwo210.jpg",
              "format": "mediumThreeByTwo210",
              "height": 140,
              "width": 210
            },
            {
              "url": "https://static01.nyt.com/images/2021/07/21/opinion/21Gordon-Reed/21Gordon-Reed-mediumThreeByTwo440.jpg",
              "format": "mediumThreeByTwo440",
              "height": 293,
              "width": 440
            }
          ]
        }
      ],
      "eta_id": 0
    },
    {
      "uri": "nyt://article/73a18252-f7ab-5968-8352-60db6d727fb1",
      "url": "https://www.nytimes.com/2020/06/17/arts/television/the-bureau-season-5-jacques-audiard.html",
      "id": 100000007186897,
      "asset_id": 100000007186897,
      "source": "New York Times",
      "published_date": "2020-06-17",
      "updated": "2020-06-30 00:54:17",
      "section": "Arts",
      "subsection": "Television",
      "nytdsection": "arts",
      "adx_keywords": "Television;Kassovitz, Mathieu (1967- );Audiard, Jacques;Rochant, Eric",
      "column": null,
      "byline": "By Elisabeth Vincentelli",
      "type": "Article",
      "title": "‘The Bureau’ Is an International Hit. Why Did Its Creator Hand It Off?",
      "abstract": "After five seasons, Éric Rochant was ready to do something new. One of them was to give Jacques Audiard near-total control over the end of his final season.",
      "des_facet": [
        "Television"
      ],
      "org_facet": [],
      "per_facet": [
        "Kassovitz, Mathieu (1967- )",
        "Audiard, Jacques",
        "Rochant, Eric"
      ],
      "geo_facet": [],
      "media": [
        {
          "type": "image",
          "subtype": "photo",
          "caption": "Mathieu Kassovitz plays the lead spy in the French series &ldquo;The Bureau,&rdquo; which has a devoted worldwide audience and has been compared favorably to the best seasons of &ldquo;Homeland.&rdquo;",
          "copyright": "Jean-Fran&ccedil;ois Baumard/Sundance Now",
          "approved_for_syndication": 1,
          "media-metadata": [
            {
              "url": "https://static01.nyt.com/images/2020/06/30/arts/17BUREAU/17BUREAU-thumbStandard.jpg",
              "format": "Standard Thumbnail",
              "height": 75,
              "width": 75
            },
            {
              "url": "https://static01.nyt.com/images/2020/06/30/arts/17BUREAU/merlin_173481837_1eefd1c6-35cf-4f3f-bd16-40222a68ea2c-mediumThreeByTwo210.jpg",
              "format": "mediumThreeByTwo210",
              "height": 140,
              "width": 210
            },
            {
              "url": "https://static01.nyt.com/images/2020/06/30/arts/17BUREAU/merlin_173481837_1eefd1c6-35cf-4f3f-bd16-40222a68ea2c-mediumThreeByTwo440.jpg",
              "format": "mediumThreeByTwo440",
              "height": 293,
              "width": 440
            }
          ]
        }
      ],
      "eta_id": 0
    }
  ]
}
""".data(using: .utf8)!
        return try! decoder.decode(MostPopularResponse.self, from: json)
    }
}
