//
//  ArticleListPresenterTests.swift
//  NicolleTanTests
//
//  Created by Jack Xiong Lim on 24/7/21.
//

import XCTest
@testable import NicolleTan

class ArticleListPresenterTests: XCTestCase {
    var sut: ArticleListPresenter!
    var mockViewModel: MockArticleListViewModel!
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        try super.setUpWithError()
        mockViewModel = MockArticleListViewModel()
        sut = ArticleListPresenter(viewController: mockViewModel)
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        sut = nil
        mockViewModel = nil
        try super.tearDownWithError()
    }
    
    func testSearchArticle_whenReturnsResult() {
        // Given
        let exp = expectation(for: NSPredicate(block: { (mockViewModel, _) -> Bool in
            return (mockViewModel as! MockArticleListViewModel).displayArticleWithViewModel
        }), evaluatedWith: mockViewModel, handler: nil)
        
        // When
        sut.presentArticleList(with: ArticleModels.ArticleModelsResponse(response: ArticleResponse.mock()))
    
        // Then
        wait(for: [exp], timeout: 2.0)
        XCTAssertTrue(mockViewModel.displayArticleWithViewModel)
    }
    
    func testMostPopular_whenReturnsResult() {
        // Given
        let exp = expectation(for: NSPredicate(block: { (mockViewModel, _) -> Bool in
            return (mockViewModel as! MockArticleListViewModel).displayArticleWithViewModel
        }), evaluatedWith: mockViewModel, handler: nil)
        
        // When
        sut.presentArticleList(with: MostPopularResponse.mock())
    
        // Then
        wait(for: [exp], timeout: 2.0)
        XCTAssertTrue(mockViewModel.displayArticleWithViewModel)
    }
    
    func testSearchArticle_whenReturnsError() {
        // Given
        let exp = expectation(for: NSPredicate(block: { (mockViewModel, _) -> Bool in
            return (mockViewModel as! MockArticleListViewModel).displayArticleWithError
        }), evaluatedWith: mockViewModel, handler: nil)
        
        // When
        sut.presentArticleList(with: ArticleModels.DataError(error: MockSearchArticleEventError.http))
        
        // Then
        wait(for: [exp], timeout: 2.0)
        XCTAssertTrue(mockViewModel.displayArticleWithError)
    }
    
    func testMostPopular_whenReturnsError() {
        // Given
        let exp = expectation(for: NSPredicate(block: { (mockViewModel, _) -> Bool in
            return (mockViewModel as! MockArticleListViewModel).displayArticleWithError
        }), evaluatedWith: mockViewModel, handler: nil)
        
        // When
        sut.presentArticleList(with: ArticleModels.DataError(error: MockMostPopularEventError.http))
        
        // Then
        wait(for: [exp], timeout: 2.0)
        XCTAssertTrue(mockViewModel.displayArticleWithError)
    }
    
}
