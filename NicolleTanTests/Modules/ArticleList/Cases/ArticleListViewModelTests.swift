//
//  ArticleListViewModelTests.swift
//  NicolleTanTests
//
//  Created by Jack Xiong Lim on 24/7/21.
//

import XCTest
@testable import NicolleTan
import RxSwift


class ArticleListViewModelTests: XCTestCase {
    var sut: ArticleListViewModel!
    var viewController: ArticleListViewController<ArticleListViewModel, MockArticleListRouter>!
    var mockInteractor: MockArticleListInteractor!
    var mockRouter: MockArticleListRouter!
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        try super.setUpWithError()
        sut = ArticleListViewModel(query: "election")
        sut = ArticleListViewModel(type: .mostEmailed)
        mockInteractor = MockArticleListInteractor()
        sut.interactor = mockInteractor
        loadViewController()
    }
    
    private func loadViewController() {
        mockRouter = MockArticleListRouter()
        viewController = ArticleListViewController(viewModel: sut, router: mockRouter)
        viewController.loadViewIfNeeded()
    }
    
    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        sut = nil
        viewController = nil
        mockInteractor = nil
        mockRouter = nil
        try super.tearDownWithError()
    }
    
    func testSearchArticle() {
        // When
        sut.getArticles(query: "election")
        
        // Then
        XCTAssertTrue(mockInteractor.retrieveSearchResultCalled)
    }
    
    func testGetMostPopularTitles() {
        // When
        sut.getMostPopularTitles()
        
        // Then
        XCTAssertTrue(mockInteractor.retrieveMostEmailedCalled)
    }
    
    func testArticleList_whenDisplayArticleListWithResponse_getCalled() {
        // When
        sut.displayArticleList(with: ArticleModels.ArticleModelsViewModels.mock())//.displayRewards(with: RewardsListModels.RewardsViewModel.mock())
        
        // Then
        XCTAssertNotNil(sut.articles.value)
    }
    
    private func givenViewIsLoadedAndNotifyErrorExpectation() -> XCTestExpectation {
        return expectation(for: NSPredicate(block: { (mockRouter, _) -> Bool in
            return (mockRouter as! MockArticleListRouter).handleError
        }), evaluatedWith: mockRouter, handler: nil)
    }
    
}
