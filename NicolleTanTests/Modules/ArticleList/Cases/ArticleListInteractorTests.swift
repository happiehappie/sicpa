//
//  ArticleListInteractorTests.swift
//  NicolleTanTests
//
//  Created by Jack Xiong Lim on 24/7/21.
//

import XCTest
@testable import NicolleTan

class ArticleListInteractorTests: XCTestCase {
    var sut: ArticleListInteractor!
    var mockPresenter: MockArticleListPresenter!
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        try super.setUpWithError()
        mockPresenter = MockArticleListPresenter()
        sut = ArticleListInteractor(presenter: mockPresenter)
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        sut = nil
        mockPresenter = nil
        try super.tearDownWithError()
    }
    
    func testArticleList_whenSearchReturnsResult() {
        // Given
        MockSearchArticleEvent.type = .someObject
        sut.searchArticleEventType = MockSearchArticleEvent.self

        // When
        let exp = expectation(for: NSPredicate(block: { (presenter, _) -> Bool in
            return (presenter as! MockArticleListPresenter).presentSearchArticleListWithResponse
        }), evaluatedWith: mockPresenter, handler: nil)
        sut.retrieveSearchResult(query: "election")

        // Then
        wait(for: [exp], timeout: 3.0)
        XCTAssertTrue(mockPresenter.presentSearchArticleListWithResponse)
    }
    
    func testArticleList_whenMostEmailedReturnsResult() {
        // Given
        MockMostPopularEvent.type = .someObject
        sut.mostEmailedArticleEventType = MockMostPopularEvent.self

        // When
        let exp = expectation(for: NSPredicate(block: { (presenter, _) -> Bool in
            return (presenter as! MockArticleListPresenter).presentMostPopularArticleListWithResponse
        }), evaluatedWith: mockPresenter, handler: nil)
        sut.retrieveMostEmailed()

        // Then
        wait(for: [exp], timeout: 3.0)
        XCTAssertTrue(mockPresenter.presentMostPopularArticleListWithResponse)
    }
    
    func testArticleList_whenMostSharedReturnsResult() {
        // Given
        MockMostPopularEvent.type = .someObject
        sut.mostSharedArticleEventType = MockMostPopularEvent.self

        // When
        let exp = expectation(for: NSPredicate(block: { (presenter, _) -> Bool in
            return (presenter as! MockArticleListPresenter).presentMostPopularArticleListWithResponse
        }), evaluatedWith: mockPresenter, handler: nil)
        sut.retrieveMostShared()

        // Then
        wait(for: [exp], timeout: 3.0)
        XCTAssertTrue(mockPresenter.presentMostPopularArticleListWithResponse)
    }
    
    func testArticleList_whenMostViewedReturnsResult() {
        // Given
        MockMostPopularEvent.type = .someObject
        sut.mostViewedArticleEventType = MockMostPopularEvent.self

        // When
        let exp = expectation(for: NSPredicate(block: { (presenter, _) -> Bool in
            return (presenter as! MockArticleListPresenter).presentMostPopularArticleListWithResponse
        }), evaluatedWith: mockPresenter, handler: nil)
        sut.retrieveMostViewed()

        // Then
        wait(for: [exp], timeout: 3.0)
        XCTAssertTrue(mockPresenter.presentMostPopularArticleListWithResponse)
    }

    func testArticleList_whenSearchReturnsError() {
        // Given
        MockSearchArticleEvent.type = .error
        sut.searchArticleEventType = MockSearchArticleEvent.self

        // When
        let exp = expectation(for: NSPredicate(block: { (presenter, _) -> Bool in
            return (presenter as! MockArticleListPresenter).presentArticleListWithError
        }), evaluatedWith: mockPresenter, handler: nil)
        sut.retrieveSearchResult(query: "election")

        // Then
        wait(for: [exp], timeout: 10.0)
        XCTAssertTrue(mockPresenter.presentArticleListWithError)
        
    }
    
    func testArticleList_whenMostPopularReturnsError() {
        // Given
        MockMostPopularEvent.type = .error
        sut.mostSharedArticleEventType = MockMostPopularEvent.self

        // When
        let exp = expectation(for: NSPredicate(block: { (presenter, _) -> Bool in
            return (presenter as! MockArticleListPresenter).presentArticleListWithError
        }), evaluatedWith: mockPresenter, handler: nil)
        sut.retrieveMostShared()

        // Then
        wait(for: [exp], timeout: 10.0)
        XCTAssertTrue(mockPresenter.presentArticleListWithError)
        
    }
    
}
