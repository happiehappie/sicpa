//
//  MockArticleListPresenter.swift
//  NicolleTanTests
//
//  Created by Jack Xiong Lim on 24/7/21.
//

import XCTest
@testable import NicolleTan

class MockArticleListPresenter: ArticleListPresentable {
    var presentSearchArticleListWithResponse = false
    var presentMostPopularArticleListWithResponse = false
    var presentArticleListWithError = false
    
    func presentArticleList(with response: ArticleModels.ArticleModelsResponse) {
        presentSearchArticleListWithResponse = true
    }
    
    func presentArticleList(with response: MostPopularResponse) {
        presentMostPopularArticleListWithResponse = true
    }
    
    func presentArticleList(with error: ArticleModels.DataError) {
        presentArticleListWithError = true
    }
}
