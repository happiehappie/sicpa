//
//  MockArticleListViewModel.swift
//  NicolleTanTests
//
//  Created by Jack Xiong Lim on 24/7/21.
//

import XCTest
@testable import NicolleTan
import RxCocoa
import RxDataSources

public class MockArticleListViewModel: ArticleListViewModelType {
    
    var getArticlesCalled = false
    var getMostPopularCalled = false
    
    var displayArticleWithViewModel = false
    var displayArticleWithError = false
    
    public var title = BehaviorRelay<String>(value: "article_list_navbar_title".localized().firstCapitalized)
    public var searchTerm = BehaviorRelay<String?>(value: nil)
    public var fetchType = BehaviorRelay<HomeViewModel.FetchType?>(value: nil)
    
    public var isLoading = BehaviorRelay<Bool>(value: true)
    
    public var articles = BehaviorRelay<[TitleSubtitleTableViewCellViewModel]>(value: [])
    
    public var dataSource: RxTableViewSectionedReloadDataSource<Section> = Section.generateDataSource()
    public var sectionedItems: BehaviorRelay<[ArticleListSection]> = BehaviorRelay(value: [])
    public var sectionCache = [Int: ArticleListSection]()
    
    public required init() {}
    
    public func getArticles(query: String) {
        getArticlesCalled = true
    }
    
    public func getMostPopularTitles() {
        getMostPopularCalled = true
    }
    
}

extension MockArticleListViewModel: ArticleListDisplayable {
    public func displayArticleList(with viewModel: ArticleModels.ArticleModelsViewModels) {
        displayArticleWithViewModel = true
    }
    
    public func displayArticleList(with error: ArticleModels.DataError) {
        displayArticleWithError = true
    }
    
}
