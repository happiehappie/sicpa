//
//  MockMostPopularEvent.swift
//  NicolleTanTests
//
//  Created by Jack Xiong Lim on 24/7/21.
//

import Foundation
import NicolleTan
import RxSwift

enum MockMostPopularEventType {
    case someObject
    case error
}

enum MockMostPopularEventError: Error {
    case http
}


class MockMostPopularEvent: SerialBusEvent<MostPopularResponse> {
    static var type: MockMostPopularEventType = .someObject
    
    override class func saga() -> [SerialSubrequest] {
        return [
            SerialSubrequest(action: { (_) -> Single<Any?> in
                switch type {
                case .someObject:
                    return Single.just(MostPopularResponse.mock()).map { $0 as Any }
                case .error:
                    return Single.error(MockMostPopularEventError.http)
                }
                
            }, debugInfo: "Mocking MostPopular Event")
        ]
    }
}
