//
//  MockSearchArticleEvent.swift
//  NicolleTanTests
//
//  Created by Jack Xiong Lim on 24/7/21.
//

import Foundation
import NicolleTan
import RxSwift

enum MockSearchArticleEventType {
    case someObject
    case error
}

enum MockSearchArticleEventError: Error {
    case http
}


class MockSearchArticleEvent: SerialBusEvent<ArticleResponse> {
    static var type: MockSearchArticleEventType = .someObject
    
    override class func saga() -> [SerialSubrequest] {
        return [
            SerialSubrequest(action: { (_) -> Single<Any?> in
                switch type {
                case .someObject:
                    return Single.just(ArticleResponse.mock()).map { $0 as Any }
                case .error:
                    return Single.error(MockSearchArticleEventError.http)
                }
                
            }, debugInfo: "Mocking SearchArticles Event")
        ]
    }
}
