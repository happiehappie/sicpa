//
//  MockArticleListInteractor.swift
//  NicolleTanTests
//
//  Created by Jack Xiong Lim on 24/7/21.
//

import XCTest
@testable import NicolleTan
import Foundation
import RxSwift

class MockArticleListInteractor: ArticleListBusinessLogic {
    var retrieveSearchResultCalled = false
    var retrieveMostEmailedCalled = false
    var retrieveMostSharedCalled = false
    var retrieveMostViewedCalled = false
    
    func retrieveSearchResult(query: String) {
        retrieveSearchResultCalled = true
    }
    
    func retrieveMostEmailed() {
        retrieveMostEmailedCalled = true
    }
    
    func retrieveMostShared() {
        retrieveMostSharedCalled = true
    }
    
    func retrieveMostViewed() {
        retrieveMostViewedCalled = true
    }
    
}
