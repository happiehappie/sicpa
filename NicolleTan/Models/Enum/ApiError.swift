//
//  ApiError.swift
//  NicolleTan
//
//  Created by Jack Xiong Lim on 21/7/21.
//

import Foundation

public enum ApiError: Error {
    case requestFailed(Int)
}
