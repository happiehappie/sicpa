//
//  ArticleResponse.swift
//  NicolleTan
//
//  Created by Jack Xiong Lim on 21/7/21.
//

import Foundation

public struct ArticleResponse: Codable {
    public let response: ArticleGeneralResponse?
}

public struct MostPopularResponse: Codable {
    public let results: [PopularArticle]?
}

public struct ArticleGeneralResponse: Codable {
    public let docs: [Document]?
}

public struct PopularArticle: Codable {
    public let title: String?
    public let published_date: String?
}

public struct Document: Codable {
    public let headline: Headline?
    public let pub_date: String?
}

public struct Headline: Codable {
    public let main: String?
}
