//
//  ArticleModels.swift
//  NicolleTan
//
//  Created by Jack Xiong Lim on 22/7/21.
//

import Foundation

enum ArticleModels {
    struct ArticleModelsResponse {
        let response: ArticleResponse
    }
    
    struct DataError {
        let error: Error
    }
    
    struct ArticleModelsViewModels {
        let articles: [TitleSubtitleTableViewCellViewModel]
    }
    
}
