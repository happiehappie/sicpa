//
//  AppRouterGeneral.swift
//  NicolleTan
//
//  Created by Jack Xiong Lim on 17/7/21.
//

import Foundation

public class AppRouterGeneral: AppRouter {
    override init() {
        super.init()
    }
    
    public override class func sharedInstance() -> AppRouterGeneral {
        struct __ { static let _sharedInstance = AppRouterGeneral() }
        return __._sharedInstance
    }
}

extension AppRouterGeneral: GeneralRoutable {
    public func navigateBack() {
        pop()
    }
}
