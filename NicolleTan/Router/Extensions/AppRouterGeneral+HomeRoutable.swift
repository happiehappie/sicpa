//
//  AppRouterGeneral+HomeRoutable.swift
//  NicolleTan
//
//  Created by Jack Xiong Lim on 19/7/21.
//

import Foundation

extension AppRouterGeneral: HomeRoutable {
    public func navigateToSearch() {
        let vm = SearchViewModel()
        let viewController = SearchViewController(viewModel: vm, router: AppRouterGeneral.sharedInstance())
        navigateTo(viewController)
    }
    
    public func navigateToArticleList(type: HomeViewModel.FetchType) {
        let vm = ArticleListViewModel(type: type)
        navigateTo(ArticleListViewController(viewModel: vm, router: AppRouterGeneral.sharedInstance()))
    }
    
}
