//
//  AppRouterGeneral+SearchRoutable.swift
//  NicolleTan
//
//  Created by Jack Xiong Lim on 21/7/21.
//

import Foundation

extension AppRouterGeneral: SearchRoutable {
    public func navigateToArticleList(query: String) {
        let vm = ArticleListViewModel(query: query)
        navigateTo(ArticleListViewController(viewModel: vm, router: AppRouterGeneral.sharedInstance()))
    }
}
