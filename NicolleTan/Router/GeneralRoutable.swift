//
//  GeneralRoutable.swift
//  NicolleTan
//
//  Created by Jack Xiong Lim on 17/7/21.
//

import Foundation

public protocol GeneralRoutable {
    func navigateBack()
}
