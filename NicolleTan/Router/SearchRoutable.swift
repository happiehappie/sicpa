//
//  SearchRoutable.swift
//  NicolleTan
//
//  Created by Jack Xiong Lim on 21/7/21.
//

import Foundation

public protocol SearchRoutable {
    func navigateToArticleList(query: String)
}
