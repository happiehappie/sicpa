//
//  HomeRoutable.swift
//  NicolleTan
//
//  Created by Jack Xiong Lim on 17/7/21.
//

import Foundation

public protocol HomeRoutable {
    func navigateToSearch()
    func navigateToArticleList(type: HomeViewModel.FetchType)
}
