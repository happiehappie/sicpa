//
//  BusEvent.swift
//  NicolleTan
//
//  Created by Jack Xiong Lim on 21/7/21.
//

import Foundation
import RxSwift

public protocol BusEvent {
    var error: Error? { get }
    static var throttle: Bool { get }
    
    init(error: Error, params: Any?)
}

public protocol SerialBusEventProtocol: BusEvent {
    associatedtype ReturnType
    static func onSubcribeAction() -> Single<ReturnType>?
    static func saga() -> [SerialSubrequest]
    
    var payload: ReturnType? { get }
    
    init(payload: ReturnType, params: Any?)
}

public protocol ConcurrentBusEventProtocol: BusEvent {
    associatedtype ReturnType
    static func saga(_ parameters: Any...) -> [ConcurrentSubrequest]
    
    var payload: ReturnType? { get }
    
    init(payload: ReturnType)
    
}

public protocol StandaloneEventProtocol: BusEvent {
    associatedtype ReturnType

    var payload: ReturnType? { get }
    
    init(payload: ReturnType)
}
