//
//  SubrequestProtocol.swift
//  NicolleTan
//
//  Created by Jack Xiong Lim on 21/7/21.
//

import Foundation
import RxSwift

protocol SubrequestProtocol {
    var compensation: Single<Void>? { get }
    var debugInfo: String { get }
}
