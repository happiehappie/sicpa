//
//  Date+Extensions.swift
//  NicolleTan
//
//  Created by Jack Xiong Lim on 22/7/21.
//

import Foundation

public extension DateFormatter {
    static let articleApiDateFormattor: DateFormatter = {
        let newDateFormatter = DateFormatter()
        newDateFormatter.locale = Locale(identifier: "en_US_POSIX")
        newDateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        return newDateFormatter
    }()
    
    static let articleDisplayDateFormattor: DateFormatter = {
        let newDateFormatter = DateFormatter()
        newDateFormatter.locale = Locale(identifier: "en_US_POSIX")
        newDateFormatter.dateFormat = "MMM d"
        return newDateFormatter
    }()
    
    static let popularArticleApiDateFormattor: DateFormatter = {
        let newDateFormatter = DateFormatter()
        newDateFormatter.locale = Locale(identifier: "en_US_POSIX")
        newDateFormatter.dateFormat = "yyyy-MM-dd"
        return newDateFormatter
    }()
    
    static let relativeDateFormatter: RelativeDateTimeFormatter = {
      let formatter = RelativeDateTimeFormatter()
      formatter.dateTimeStyle = .named
      formatter.unitsStyle = .short
      return formatter
    }()
}
