//
//  String+Extensions.swift
//  NicolleTan
//
//  Created by Jack Xiong Lim on 21/7/21.
//

import Foundation

public extension String {
    
    var firstCapitalized: String { prefix(1).capitalized + dropFirst() }
    
    func localized(_ lang: String = "en") -> String {
        guard let path = Bundle.main.path(forResource: lang, ofType: "lproj") else {
            return self
        }
        guard let bundle = Bundle(path: path) else {
            return self
        }
        return NSLocalizedString(self, tableName: nil, bundle: bundle, value: "", comment: "")
    }
    
    func localizedCapitalized() -> String {
        return localized().capitalized
    }
    
    func localizedUppercased() -> String {
        return localized().localizedUppercase
    }
    
}
