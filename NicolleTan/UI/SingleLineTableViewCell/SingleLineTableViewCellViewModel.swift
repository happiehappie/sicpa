//
//  SingleLIneTableViewCellViewModel.swift
//  NicolleTan
//
//  Created by Jack Xiong Lim on 21/7/21.
//

import Foundation
import RxDataSources
import RxCocoa
import RxSwift

public protocol SingleLIneTableViewCellViewModelType {
    var titleText: BehaviorRelay<String?> { get }
    var fetchType: BehaviorRelay<HomeViewModel.FetchType?> { get }
    var accessoryType: BehaviorRelay<UITableViewCell.AccessoryType> { get }
    
    var topConstraintValue: BehaviorRelay<CGFloat?> { get }
    
    var isLabelVerticallyCentered: BehaviorRelay<Bool> { get }
    
    var cellHeight: BehaviorRelay<CGFloat?> { get }
    
    init()
}

public class SingleLineTableViewCellViewModel: SingleLIneTableViewCellViewModelType {
    
    public let titleText = BehaviorRelay<String?>(value: nil)
    public let fetchType = BehaviorRelay<HomeViewModel.FetchType?>(value: nil)
    public let accessoryType = BehaviorRelay<UITableViewCell.AccessoryType>(value: .none)
    
    public let topConstraintValue = BehaviorRelay<CGFloat?>(value: nil)
    
    public let isLabelVerticallyCentered = BehaviorRelay<Bool>(value: false)
    
    public let cellHeight = BehaviorRelay<CGFloat?>(value: nil)
    
    required public init() {}
    
    
    convenience public init(text: String?, type: HomeViewModel.FetchType?, accessoryType: UITableViewCell.AccessoryType = .none) {
        self.init()
        titleText.accept(text)
        fetchType.accept(type)
        self.accessoryType.accept(accessoryType)
    }
    
}
