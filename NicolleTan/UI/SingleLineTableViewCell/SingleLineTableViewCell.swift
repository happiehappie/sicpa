//
//  SingleLIneTableViewCell.swift
//  NicolleTan
//
//  Created by Jack Xiong Lim on 21/7/21.
//

import RxSwift

public class SingleLineTableViewCell: GeneralTableViewCell {
    
    lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    public lazy var titleLabel: UILabel = {
        let newLabel = UILabel()
        newLabel.translatesAutoresizingMaskIntoConstraints = false
        return newLabel
    }()
    
    public var titleLabelTopConstraint: NSLayoutConstraint!
    public var titleLabelBottomConstraint: NSLayoutConstraint!
    public var titleLabelCenterYConstraint: NSLayoutConstraint!
    public var containerViewHeightConstraint: NSLayoutConstraint?
    
    private var disposeBag: DisposeBag!
    public var viewModel: SingleLIneTableViewCellViewModelType = SingleLineTableViewCellViewModel()
    
    // MARK: - Initializer and Lifecycle Methods -

    public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: reuseIdentifier)
        
        setupView()
        setupListeners()
    }

    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override public func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = nil
    }
    
    public override func configureWith(value: Any) {
        guard let value = value as? SingleLIneTableViewCellViewModelType else { return }
        
        viewModel = value
        setupListeners()
    }
    // MARK: - Private API -
    
    private func setupView() {
        selectionStyle = .none
        accessoryType = .disclosureIndicator
        addSubview(containerView)
        containerView.addSubview(titleLabel)
        
        titleLabelCenterYConstraint = titleLabel.centerYAnchor.constraint(equalTo: containerView.centerYAnchor)
        titleLabelBottomConstraint = titleLabel.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -4)
        titleLabelCenterYConstraint.identifier = "titleLabelCenterYConstraint"
        containerViewHeightConstraint = containerView.heightAnchor.constraint(equalToConstant: viewModel.cellHeight.value ?? 44)
        containerViewHeightConstraint?.identifier = "containerViewHeightConstraint"
        containerViewHeightConstraint?.priority = UILayoutPriority(999)
        NSLayoutConstraint.activate([
            
            containerView.topAnchor.constraint(equalTo: topAnchor),
            containerView.leadingAnchor.constraint(equalTo: leadingAnchor),
            containerView.trailingAnchor.constraint(equalTo: trailingAnchor),
            containerView.bottomAnchor.constraint(equalTo: bottomAnchor),
            
            
            titleLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 16)
            
        ])
        
    }
    
    private func setupListeners() {
        disposeBag = DisposeBag()
        
        viewModel.titleText
            .subscribe(onNext: { [weak self] (value) in
                guard let strongSelf = self else { return }
                strongSelf.titleLabel.text = value
            })
            .disposed(by: disposeBag)
        
        viewModel.accessoryType
            .subscribe(onNext: { [weak self] (value) in
                guard let strongSelf = self else { return }
                strongSelf.accessoryType = value
            })
            .disposed(by: disposeBag)
        
        viewModel.isLabelVerticallyCentered
            .subscribe(onNext: { [weak self] (value) in
                guard let strongSelf = self else { return }
                
                strongSelf.titleLabelTopConstraint?.isActive = !value
                strongSelf.titleLabelBottomConstraint?.isActive = !value
                strongSelf.titleLabelCenterYConstraint.isActive = value
                strongSelf.containerViewHeightConstraint?.constant = strongSelf.viewModel.cellHeight.value ?? 44
                strongSelf.containerViewHeightConstraint?.isActive = value
                
                strongSelf.layoutIfNeeded()
            })
            .disposed(by: disposeBag)
        
        viewModel.cellHeight
            .subscribe(onNext: { [weak self] (value) in
                guard let strongSelf = self, let height = value, strongSelf.viewModel.isLabelVerticallyCentered.value else { return }
                strongSelf.titleLabelTopConstraint?.isActive = false
                strongSelf.titleLabelBottomConstraint?.isActive = false
                strongSelf.containerViewHeightConstraint?.constant = height
                strongSelf.containerViewHeightConstraint?.isActive = true
                strongSelf.titleLabelCenterYConstraint.isActive = true
                
                strongSelf.layoutIfNeeded()
            })
            .disposed(by: disposeBag)
        
        viewModel.topConstraintValue
            .subscribe(onNext: { [weak self] (value) in
                guard let strongSelf = self, let value = value else { return }
                strongSelf.containerViewHeightConstraint?.isActive = false
                strongSelf.titleLabelCenterYConstraint?.isActive = false
                
                strongSelf.titleLabelTopConstraint = strongSelf.titleLabel.topAnchor.constraint(equalTo: strongSelf.containerView.topAnchor, constant: value)
                strongSelf.titleLabelTopConstraint.isActive = true
                strongSelf.titleLabelBottomConstraint.isActive = true
                
                strongSelf.layoutIfNeeded()
            })
            .disposed(by: disposeBag)
        
    }
}
