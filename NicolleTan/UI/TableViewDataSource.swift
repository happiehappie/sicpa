//
//  TableViewDataSource.swift
//  NicolleTan
//
//  Created by Jack Xiong Lim on 19/7/21.
//

import Foundation
import RxDataSources

public protocol TableViewDataSource: CaseIterable {
    associatedtype Section where Section: SectionModelType, Section == Self
    var cellType: UITableViewCell.Type { get }
    static func generateDataSource() -> RxTableViewSectionedReloadDataSource<Section>
}

