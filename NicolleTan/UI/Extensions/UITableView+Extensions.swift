//
//  UITableView+Extensions.swift
//  NicolleTan
//
//  Created by Jack Xiong Lim on 19/7/21.
//

import UIKit

public extension UITableView {
    func registerCellClass <CellClass: UITableViewCell> (_ cellClass: CellClass.Type) {
        register(cellClass, forCellReuseIdentifier: String(describing: cellClass))
    }
    
    func dequeueCell<CellClass: UITableViewCell>(_ cellClass: CellClass.Type, at indexPath: IndexPath) -> CellClass {
        return dequeueReusableCell(withIdentifier: String(describing: cellClass), for: indexPath) as! CellClass
    }
    
    @discardableResult
    func registerCellNibForClass(_ cellClass: AnyClass) -> UINib {
        let classNameWithoutModule = cellClass
            .description()
            .components(separatedBy: ".")
            .dropFirst()
            .joined(separator: ".")
        
        let nib = UINib(nibName: classNameWithoutModule, bundle: Bundle(for: cellClass))
        register(nib, forCellReuseIdentifier: classNameWithoutModule)
        return nib
    }
    
    func scrollToBottom(animated: Bool = true) {
        let sections = self.numberOfSections
        let rows = self.numberOfRows(inSection: sections - 1)
        if rows > 0 {
            self.scrollToRow(at: IndexPath(row: rows - 1, section: sections - 1), at: .bottom, animated: animated)
        }
    }
    
    func scrollToTop() {
        if #available(iOS 11.0, *) {
            self.setContentOffset(CGPoint(x: 0, y: -self.adjustedContentInset.top), animated: true)
        } else {
            self.setContentOffset(CGPoint(x: 0, y: -self.contentInset.top), animated: true)
        }
    }
}
