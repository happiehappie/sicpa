//
//  ValueCellDataSource.swift
//  NicolleTan
//
//  Created by Jack Xiong Lim on 19/7/21.
//

import Foundation
import UIKit

open class GeneralTableViewCell: UITableViewCell, ValueCell {
    @objc open func configureWith(value: Any) {
    }
}
