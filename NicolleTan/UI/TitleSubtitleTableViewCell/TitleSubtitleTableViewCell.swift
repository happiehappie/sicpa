//
//  TitleSubtitleTableViewCell.swift
//  NicolleTan
//
//  Created by Jack Xiong Lim on 22/7/21.
//

import RxSwift

public class TitleSubtitleTableViewCell: GeneralTableViewCell {
    lazy var containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    public lazy var titleLabel: UILabel = {
        let newLabel = UILabel()
        newLabel.numberOfLines = 0
        newLabel.lineBreakMode = .byWordWrapping
        newLabel.translatesAutoresizingMaskIntoConstraints = false
        return newLabel
    }()
    
    public lazy var subtitleLabel: UILabel = {
        let newLabel = UILabel()
        newLabel.translatesAutoresizingMaskIntoConstraints = false
        return newLabel
    }()
    
    private var disposeBag: DisposeBag!
    public var viewModel: TitleSubtitleTableViewCellViewModelType = TitleSubtitleTableViewCellViewModel()
    // MARK: - Initializer and Lifecycle Methods -
    
    public override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: reuseIdentifier)
        
        setupView()
        setupListeners()
    }
    
    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = nil
    }
    
    public override func configureWith(value: Any) {
        guard let value = value as? TitleSubtitleTableViewCellViewModelType else { return }
        
        viewModel = value
        setupListeners()
    }
    
    private func setupView() {
        selectionStyle = .none
        addSubview(containerView)
        containerView.addSubview(titleLabel)
        containerView.addSubview(subtitleLabel)
        
        NSLayoutConstraint.activate([
            
            containerView.topAnchor.constraint(equalTo: topAnchor),
            containerView.leadingAnchor.constraint(equalTo: leadingAnchor),
            containerView.trailingAnchor.constraint(equalTo: trailingAnchor),
            containerView.bottomAnchor.constraint(equalTo: bottomAnchor),
            
            titleLabel.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 16),
            titleLabel.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 8),
            titleLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -16),
            
            subtitleLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 4),
            subtitleLabel.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor),
            subtitleLabel.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -16)
            
        ])
    }
    
    private func setupListeners() {
        disposeBag = DisposeBag()
        
        viewModel.titleText
            .subscribe(onNext: { [weak self] (value) in
                guard let strongSelf = self else { return }
                strongSelf.titleLabel.text = value
            })
            .disposed(by: disposeBag)
        
        viewModel.subtitleText
            .subscribe(onNext: { [weak self] (value) in
                guard let strongSelf = self else { return }
                strongSelf.subtitleLabel.text = value
            })
            .disposed(by: disposeBag)
    }
    
}
