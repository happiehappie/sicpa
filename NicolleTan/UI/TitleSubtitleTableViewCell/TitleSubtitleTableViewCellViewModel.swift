//
//  TitleSubtitleTableViewCellViewModel.swift
//  NicolleTan
//
//  Created by Jack Xiong Lim on 22/7/21.
//

import Foundation
import RxDataSources
import RxCocoa
import RxSwift

public protocol TitleSubtitleTableViewCellViewModelType {
    var titleText: BehaviorRelay<String?> { get }
    var subtitleText: BehaviorRelay<String?> { get }
    init()
}

public class TitleSubtitleTableViewCellViewModel: TitleSubtitleTableViewCellViewModelType {
    public let titleText = BehaviorRelay<String?>(value: nil)
    public let subtitleText = BehaviorRelay<String?>(value: nil)
    required public init() {}
}
