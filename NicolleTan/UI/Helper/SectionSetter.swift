//
//  SectionSetter.swift
//  NicolleTan
//
//  Created by Jack Xiong Lim on 19/7/21.
//

import Foundation
import RxCocoa
import RxDataSources

public protocol RelativeOrder {
    var sectionOrder: Int { get }
}

public protocol SectionSetter: AnyObject {
    associatedtype Section: SectionModelType & RelativeOrder
    var sectionedItems: BehaviorRelay<[Section]> { get }
    var sectionCache: [Int: Section] { get set}
        
    func setSection(_ section: Section)
}

public protocol SectionSetterPlus {
    associatedtype Section: SectionModelType & RelativeOrder
    var dataSource: RxTableViewSectionedReloadDataSource<Section> { get }
}

public extension SectionSetter {
    func setSection(_ section: Section) {
        sectionCache[section.sectionOrder] = section
        
        let sortedSection = sectionCache.sorted(by: { $0.0 < $1.0 }).map { $0.1 }
        sectionedItems.accept(sortedSection)
    }
}

public protocol TableViewSectionSetter: AnyObject {
    associatedtype Section: SectionModelType & RelativeOrder
    var dataSource: RxTableViewSectionedReloadDataSource<Section> { get }
}

public protocol CollectionViewSectionSetter: AnyObject {
    associatedtype Section: SectionModelType & RelativeOrder
    var dataSource: RxCollectionViewSectionedReloadDataSource<Section> { get }
}

public enum SectionMismatchError: Error {
    case missingSelf
}

