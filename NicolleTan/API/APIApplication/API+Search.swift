//
//  API+Search.swift
//  NicolleTan
//
//  Created by Jack Xiong Lim on 21/7/21.
//

import Foundation
import RxSwift

public extension API {
    static func getCampaignList(query: String) -> Single<ArticleResponse> {
        return API.request(.get, "\(baseUrl)/search/v2/articlesearch.json?q=\(query)&api-key=\(token)")
    }
    static func getMostEmailedList(days: Int) -> Single<MostPopularResponse> {
        return API.request(.get, "\(baseUrl)/mostpopular/v2/emailed/\(days).json?api-key=\(token)")
    }
    static func getMostSharedList(days: Int) -> Single<MostPopularResponse> {
        return API.request(.get, "\(baseUrl)/mostpopular/v2/shared/\(days).json?api-key=\(token)")
    }
    static func getMostViewedList(days: Int) -> Single<MostPopularResponse> {
        return API.request(.get, "\(baseUrl)/mostpopular/v2/viewed/\(days).json?api-key=\(token)")
    }
}
