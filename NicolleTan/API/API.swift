//
//  API.swift
//  NicolleTan
//
//  Created by Jack Xiong Lim on 21/7/21.
//

import Foundation
import Alamofire
import RxAlamofire
import RxSwift
import SwiftyJSON

public class API: APISuperClass {
    
    public static func request<T: Decodable>(sessionManager: Session = Session.default,
                                             _ method: HTTPMethod,
                                             _ urlString: String,
                                             headers: HTTPHeaders? = nil,
                                             parameters: [String: Any]? = nil,
                                             shouldShowLog: Bool = false) -> Single<T> {
        guard let url = URL(string: urlString) else {
            return Single.error(HTTPRequestError.invalidURL)
        }
        return sessionManager.rx
            .request(method,
                     url,
                     parameters: parameters,
                     encoding: method == .get ? URLEncoding.default : JSONEncoding.default,
                     headers: headers)
            .responseJSON()
            .do(onNext: { (dataResponse) in
                try httpErrorHandler(dataResponse: dataResponse)
            })
            .map { (dataResponse) -> T in
                guard let data = dataResponse.data else { throw HTTPRequestError.dataNotFound }
                if shouldShowLog {var json: JSON!
                    do {
                        json = try JSON(data: data)
                    } catch {
                        json = JSON([String: Any]())
                    }
                    print(json ?? "")
                    
                }
                let payload = try JSONDecoder().decode(T.self, from: data)
                return payload
            }
            .observe(on: MainScheduler.instance)
            .asSingle()
    }
    
    private static func httpErrorHandler(dataResponse: DataResponse<Any, AFError>) throws {
        guard let code = dataResponse.response?.statusCode, 200..<300 ~= code else {
            let statusCode = dataResponse.response?.statusCode ?? 0
            throw ApiError.requestFailed(statusCode)
            
        }
        
    }
    
}
