//
//  APISuperClass.swift
//  NicolleTan
//
//  Created by Jack Xiong Lim on 21/7/21.
//

import Foundation
import Alamofire
import RxAlamofire
import RxSwift

public enum HTTPRequestError: Error {
    case unauthorizedAccess
    case invalidURL
    case dataNotFound
}

open class APISuperClass {
    
    // MARK: - Properties -
    public static var baseUrl: String {
        return "https://api.nytimes.com/svc"
    }
    public static var token: String {
        return "GnNObB2tBbuOjYUynYfKLGU1qbjzEbhg"
    }
}
