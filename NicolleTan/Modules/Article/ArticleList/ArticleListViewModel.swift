//
//  ArticleListViewModel.swift
//  NicolleTan
//
//  Created by Jack Xiong Lim on 22/7/21.
//

import Foundation
import RxCocoa
import RxDataSources

protocol ArticleListViewModelType: SectionSetter, TableViewSectionSetter where Section == ArticleListSection {
    
    var title: BehaviorRelay<String> { get }
    var searchTerm: BehaviorRelay<String?> { get }
    var fetchType: BehaviorRelay<HomeViewModel.FetchType?> { get }
    
    var isLoading: BehaviorRelay<Bool> { get }
    
    var articles: BehaviorRelay<[TitleSubtitleTableViewCellViewModel]> { get }
    
    func getArticles(query: String)
    func getMostPopularTitles()
    
    init()
}

public class ArticleListViewModel: ArticleListViewModelType {
    
    public var title = BehaviorRelay<String>(value: "article_list_navbar_title".localized().firstCapitalized)
    public var searchTerm = BehaviorRelay<String?>(value: nil)
    public var fetchType = BehaviorRelay<HomeViewModel.FetchType?>(value: nil)
    
    public var isLoading = BehaviorRelay<Bool>(value: true)
    
    public var articles = BehaviorRelay<[TitleSubtitleTableViewCellViewModel]>(value: [])
    
    public var dataSource: RxTableViewSectionedReloadDataSource<Section> = Section.generateDataSource()
    public var sectionedItems: BehaviorRelay<[ArticleListSection]> = BehaviorRelay(value: [])
    public var sectionCache = [Int: ArticleListSection]()
    
    var interactor: ArticleListBusinessLogic!
    
    required public init() {
        interactor = ArticleListInteractor(presenter: ArticleListPresenter(viewController: self))
    }
    
    convenience init(query: String) {
        self.init()
        searchTerm.accept(query)
    }
    
    convenience init(type: HomeViewModel.FetchType) {
        self.init()
        fetchType.accept(type)
    }
    
    public func getArticles(query: String) {
        interactor.retrieveSearchResult(query: query)
    }
    
    public func getMostPopularTitles() {
        guard let type = fetchType.value else { return  }
        switch type {
        case .mostEmailed:
            interactor.retrieveMostEmailed()
        case .mostShared:
            interactor.retrieveMostShared()
        case .mostViewed:
            interactor.retrieveMostViewed()
        default:
            break
        }
    }
    
}

extension ArticleListViewModel: ArticleListDisplayable {
    func displayArticleList(with viewModel: ArticleModels.ArticleModelsViewModels) {
        isLoading.accept(false)
        articles.accept(viewModel.articles)
    }
    
    func displayArticleList(with error: ArticleModels.DataError) {
        // error handling
    }
    
    
}
