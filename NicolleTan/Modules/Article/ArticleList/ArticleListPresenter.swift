//
//  ArticleListPresenter.swift
//  NicolleTan
//
//  Created by Jack Xiong Lim on 22/7/21.
//

import Foundation

struct ArticleListPresenter {
    private(set) weak var viewController: ArticleListDisplayable?
    
    init(viewController: ArticleListDisplayable) {
        self.viewController = viewController
    }
}

extension ArticleListPresenter: ArticleListPresentable {
    func presentArticleList(with response: MostPopularResponse) {
        guard let articles = response.results else { return  }
        if articles.count > 0 {
            var queriedArticles = [TitleSubtitleTableViewCellViewModel]()
            for article in articles {
                let vm = TitleSubtitleTableViewCellViewModel()
                vm.titleText.accept(article.title)
                
                if let date = DateFormatter.popularArticleApiDateFormattor.date(from: article.published_date ?? "") {
                    vm.subtitleText.accept(DateFormatter.relativeDateFormatter.localizedString(for: date, relativeTo: Date()))
                }
                queriedArticles.append(vm)
            }
            viewController?.displayArticleList(with: ArticleModels.ArticleModelsViewModels(articles: queriedArticles))
        } else {
            // can potentially show background view
        }
    }
    
    func presentArticleList(with response: ArticleModels.ArticleModelsResponse) {
        guard let articles = response.response.response?.docs else { return }
        
        if articles.count > 0 {
            var queriedArticles = [TitleSubtitleTableViewCellViewModel]()
            for article in articles {
                let vm = TitleSubtitleTableViewCellViewModel()
                vm.titleText.accept(article.headline?.main)
                if let date = DateFormatter.articleApiDateFormattor.date(from: article.pub_date ?? "") {
                    vm.subtitleText.accept(DateFormatter.articleDisplayDateFormattor.string(from: date))
                }
                queriedArticles.append(vm)
            }
            viewController?.displayArticleList(with: ArticleModels.ArticleModelsViewModels(articles: queriedArticles))
        } else {
            // can potentially show background view
        }
        
    }
    
    func presentArticleList(with error: ArticleModels.DataError) {
        // error handling
        viewController?.displayArticleList(with: error)
    }
    
    
}
