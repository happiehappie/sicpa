//
//  ArticleListSection.swift
//  NicolleTan
//
//  Created by Jack Xiong Lim on 22/7/21.
//

import Foundation
import RxDataSources

public enum ArticleListSection: SectionModelType {
    case articles(items: [TitleSubtitleTableViewCellViewModel])
    
    public var items: [AnyObject] {
        switch self {
        case .articles(let items):
            return items
        }
    }
    public init(original: ArticleListSection, items: [AnyObject]) {
        switch original {
        case .articles:
            self = .articles(items: items as! [TitleSubtitleTableViewCellViewModel])
        }
    }
}

extension ArticleListSection: TableViewDataSource {
    
    public typealias Section = ArticleListSection
    public static var allCases: [ArticleListSection] {
        return [
            .articles(items: [TitleSubtitleTableViewCellViewModel]())
        ]
    }
    
    public static func generateDataSource() -> RxTableViewSectionedReloadDataSource<ArticleListSection> {
        return RxTableViewSectionedReloadDataSource<ArticleListSection>(configureCell: { (_, tableView, indexPath, viewModel) -> UITableViewCell in
            var cell: UITableViewCell!
            if let viewModel = viewModel as? TitleSubtitleTableViewCellViewModel {
                let newCell = tableView.dequeueCell(TitleSubtitleTableViewCell.self, at: indexPath)
                newCell.configureWith(value: viewModel)
                cell = newCell
            }
            
            return cell
        })
    }
    public var cellType: UITableViewCell.Type {
        switch self {
        case .articles:
            return TitleSubtitleTableViewCell.self
        }
    }
}

extension ArticleListSection: RelativeOrder {
    public var sectionOrder: Int {
        switch self {
        case .articles:
            return 0
        }
    }
}
