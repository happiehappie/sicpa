//
//  ArticleListInterfaces.swift
//  NicolleTan
//
//  Created by Jack Xiong Lim on 22/7/21.
//

import Foundation

protocol ArticleListDisplayable: AnyObject {
    func displayArticleList(with viewModel: ArticleModels.ArticleModelsViewModels)
    func displayArticleList(with error: ArticleModels.DataError)
}

protocol ArticleListBusinessLogic { // Interactor
    func retrieveSearchResult(query: String)
    func retrieveMostEmailed()
    func retrieveMostShared()
    func retrieveMostViewed()
}

protocol ArticleListPresentable {
    func presentArticleList(with response: ArticleModels.ArticleModelsResponse)
    func presentArticleList(with response: MostPopularResponse)
    func presentArticleList(with error: ArticleModels.DataError)
}
