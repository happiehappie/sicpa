//
//  ArticleListViewController.swift
//  NicolleTan
//
//  Created by Jack Xiong Lim on 22/7/21.
//

import UIKit
import RxSwift

class ArticleListViewController<ViewModel, Router>: UIViewController where ViewModel: ArticleListViewModelType, Router: ArticleListRoutable {
    
    var rootView: ArticleListView {
        return view as! ArticleListView
    }
    
    private lazy var loadingIndicatorView: UIActivityIndicatorView = {
        let newIndicatorView = UIActivityIndicatorView(style: .medium)
        return newIndicatorView
    }()
    
    private(set) lazy var viewModel: ViewModel = ViewModel()
    private var router: Router
    private var disposeBag: DisposeBag!
    
    public init(viewModel: ViewModel, router: Router) {
        self.router = router
        
        super.init(nibName: nil, bundle: nil)
        
        self.viewModel = viewModel
    }
    
    public override func loadView() {
        view = ArticleListView(frame: UIScreen.main.bounds)
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AppRouterGeneral.sharedInstance().navigationController = navigationController
        setupView()
        setupListeners()
    }
    
    // MARK: - Private API -
    // MARK: Setup Methods
    
    private func setupView() {
        
        rootView.tableView.backgroundView = loadingIndicatorView
        
        for section in ArticleListSection.allCases {
            rootView.tableView.registerCellClass(section.cellType)
        }
    }
    
    private func setupListeners() {
        disposeBag = DisposeBag()
        
        viewModel.sectionedItems
            .bind(to: rootView.tableView.rx.items(dataSource: viewModel.dataSource))
            .disposed(by: disposeBag)
        
        viewModel.title
            .subscribe(onNext: { [weak self] (value) in
                guard let strongSelf = self else { return }
                strongSelf.title = value
            })
            .disposed(by: disposeBag)
        
        viewModel.searchTerm
            .subscribe(onNext: { [weak self] in
                guard let strongSelf = self, let value = $0 else { return }
                strongSelf.viewModel.getArticles(query: value)
            })
            .disposed(by: disposeBag)
        
        viewModel.fetchType
            .subscribe(onNext: { [weak self] value in
                guard let strongSelf = self else { return }
                strongSelf.viewModel.getMostPopularTitles()
            })
            .disposed(by: disposeBag)
        
        viewModel.isLoading
            .subscribe(onNext: { [weak self] in
                guard let strongSelf = self else { return }
                $0 ? strongSelf.loadingIndicatorView.startAnimating() : strongSelf.loadingIndicatorView.stopAnimating()
            })
            .disposed(by: disposeBag)
        
        viewModel.articles
            .subscribe(onNext: { [weak self] (value) in
                guard let strongSelf = self else { return }
                strongSelf.viewModel.setSection(.articles(items: value))
            })
            .disposed(by: disposeBag)
        
        
    }
    
}
