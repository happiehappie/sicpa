//
//  ArticleListInteractor.swift
//  NicolleTan
//
//  Created by Jack Xiong Lim on 22/7/21.
//

import Foundation
import RxSwift

class ArticleListInteractor: ArticleListBusinessLogic {
    var presenter: ArticleListPresentable?
    var fetchType: HomeViewModel.FetchType?
    var searchArticleEventType: SerialBusEvent.Type = SearchArticleEvent.self
    var mostEmailedArticleEventType: SerialBusEvent.Type = MostEmailedArticleEvent.self
    var mostSharedArticleEventType: SerialBusEvent.Type = MostSharedArticleEvent.self
    var mostViewedArticleEventType: SerialBusEvent.Type = MostViewedArticleEvent.self
    private var disposeBag = DisposeBag()
    
    required init(presenter: ArticleListPresentable) {
        self.presenter = presenter
        EventBus.shared.events(of: searchArticleEventType)
            .subscribe(onNext: { [weak self] value in
                guard let strongSelf = self else { return }
                if let payload = value.payload {
                    print("search")
                    strongSelf.presenter?.presentArticleList(with: ArticleModels.ArticleModelsResponse(response: payload))
                } else if let error = value.error {
                    strongSelf.presenter?.presentArticleList(with: ArticleModels.DataError(error: error))
                }
            })
            .disposed(by: disposeBag)
        
        EventBus.shared.events(of: mostEmailedArticleEventType)
            .subscribe(onNext: { [weak self] value in
                guard let strongSelf = self, let type = strongSelf.fetchType, type == .mostEmailed else { return }
                if let payload = value.payload {
                    print("email")
                    strongSelf.presenter?.presentArticleList(with: payload)
                } else if let error = value.error {
                    strongSelf.presenter?.presentArticleList(with: ArticleModels.DataError(error: error))
                }
            })
            .disposed(by: disposeBag)
        
        EventBus.shared.events(of: mostSharedArticleEventType)
            .subscribe(onNext: { [weak self] value in
                guard let strongSelf = self, let type = strongSelf.fetchType, type == .mostShared else { return }
                if let payload = value.payload {
                    print("shared")
                    strongSelf.presenter?.presentArticleList(with: payload)
                } else if let error = value.error {
                    strongSelf.presenter?.presentArticleList(with: ArticleModels.DataError(error: error))
                }
            })
            .disposed(by: disposeBag)
        
        EventBus.shared.events(of: mostViewedArticleEventType)
            .subscribe(onNext: { [weak self] value in
                guard let strongSelf = self, let type = strongSelf.fetchType, type == .mostViewed else { return }
                if let payload = value.payload {
                    print("viewed")
                    strongSelf.presenter?.presentArticleList(with: payload)
                } else if let error = value.error {
                    strongSelf.presenter?.presentArticleList(with: ArticleModels.DataError(error: error))
                }
            })
            .disposed(by: disposeBag)
    }
    
    func retrieveSearchResult(query: String) {
        EventBus.shared.executeEvent(of: searchArticleEventType, parameters: query)
    }
    func retrieveMostEmailed() {
        fetchType = .mostEmailed
        EventBus.shared.executeEvent(of: mostEmailedArticleEventType)
    }
    func retrieveMostShared() {
        fetchType = .mostShared
        EventBus.shared.executeEvent(of: mostSharedArticleEventType)
    }
    func retrieveMostViewed() {
        fetchType = .mostViewed
        EventBus.shared.executeEvent(of: mostViewedArticleEventType)
    }
}
