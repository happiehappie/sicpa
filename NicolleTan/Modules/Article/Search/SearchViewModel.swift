//
//  SearchViewModel.swift
//  NicolleTan
//
//  Created by Jack Xiong Lim on 21/7/21.
//

import Foundation
import RxCocoa
import RxDataSources

protocol SearchViewModelType {
    var title: BehaviorRelay<String> { get }
    init()
}

public class SearchViewModel: SearchViewModelType {
    
    public var title = BehaviorRelay<String>(value: "search_text".localized().firstCapitalized)
    required public init() {
    }
}
