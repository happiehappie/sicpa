//
//  SearchViewController.swift
//  NicolleTan
//
//  Created by Jack Xiong Lim on 21/7/21.
//

import UIKit
import RxSwift
import RxCocoa

class SearchViewController<ViewModel, Router>: UIViewController where ViewModel: SearchViewModelType, Router: SearchRoutable {
    
    var rootView: SearchView {
        return view as! SearchView
    }
    
    private(set) lazy var viewModel: ViewModel = ViewModel()
    private var router: Router
    private var disposeBag: DisposeBag!
    
    public init(viewModel: ViewModel, router: Router) {
        self.router = router
        
        super.init(nibName: nil, bundle: nil)
        
        self.viewModel = viewModel
    }
    
    public override func loadView() {
        view = SearchView(frame: UIScreen.main.bounds)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupListeners()
    }
    
    // MARK: - Private API -
    // MARK: Setup Methods
    
    private func setupListeners() {
        disposeBag = DisposeBag()
        
        viewModel.title
            .subscribe(onNext: { [weak self] (value) in
                guard let strongSelf = self else { return }
                strongSelf.title = value
            })
            .disposed(by: disposeBag)
        
        rootView.searchTextField
            .rx
            .text
            .throttle(.milliseconds(100), scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] (value) in
                guard let strongSelf = self else { return }
                strongSelf.rootView.searchButton.isEnabled = !(value ?? "").isEmpty
            })
            .disposed(by: disposeBag)
        rootView.searchButton.rx.tap
            .throttle(.milliseconds(100), scheduler: MainScheduler.instance)
            .subscribe(onNext: { [weak self] (value) in
                guard let strongSelf = self, let text = strongSelf.rootView.searchTextField.text else { return }
                strongSelf.rootView.searchTextField.resignFirstResponder()
                strongSelf.router.navigateToArticleList(query: text)
            })
            .disposed(by: disposeBag)
    }
}
