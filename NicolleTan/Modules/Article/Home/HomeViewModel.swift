//
//  HomeViewModel.swift
//  NicolleTan
//
//  Created by Jack Xiong Lim on 17/7/21.
//

import Foundation
import RxCocoa
import RxDataSources

protocol HomeViewModelType: SectionSetter, TableViewSectionSetter where Section == HomeSection {
    var title: BehaviorRelay<String> { get }
    var searchTitle: BehaviorRelay<SingleLineTableViewCellViewModel?> { get }
    var searchCell: BehaviorRelay<SingleLineTableViewCellViewModel?> { get }
    var popularTitle: BehaviorRelay<SingleLineTableViewCellViewModel?> { get }
    var popularCells: BehaviorRelay<[SingleLineTableViewCellViewModel]> { get }
    init()
}

public class HomeViewModel: HomeViewModelType {
    
    public enum FetchType {
        case search
        case mostViewed
        case mostShared
        case mostEmailed
    }
    
    public var title = BehaviorRelay<String>(value: "home_navbar_title".localized().localizedUppercase)
    public var searchTitle = BehaviorRelay<SingleLineTableViewCellViewModel?>(value: nil)
    public var searchCell = BehaviorRelay<SingleLineTableViewCellViewModel?>(value: nil)
    public var popularTitle = BehaviorRelay<SingleLineTableViewCellViewModel?>(value: nil)
    public var popularCells = BehaviorRelay<[SingleLineTableViewCellViewModel]>(value: [])
    
    public var dataSource: RxTableViewSectionedReloadDataSource<Section> = Section.generateDataSource()
    public var sectionedItems: BehaviorRelay<[HomeSection]> = BehaviorRelay(value: [])
    public var sectionCache = [Int: HomeSection]()
    
    required public init() {
        let searchTitleVM = SingleLineTableViewCellViewModel()
        searchTitleVM.titleText.accept("search_text".localizedCapitalized())
        searchTitleVM.topConstraintValue.accept(32)
        searchTitle.accept(searchTitleVM)
        
        let searchCellVM = SingleLineTableViewCellViewModel()
        searchCellVM.fetchType.accept(.search)
        searchCellVM.titleText.accept("search_articles_text".localizedCapitalized())
        searchCellVM.accessoryType.accept(.disclosureIndicator)
        searchCellVM.isLabelVerticallyCentered.accept(true)
        searchCell.accept(searchCellVM)
        
        let popularTitleVM = SingleLineTableViewCellViewModel()
        popularTitleVM.titleText.accept("popular_text".localizedCapitalized())
        
        popularTitleVM.topConstraintValue.accept(32)
        popularTitle.accept(popularTitleVM)
        
        let mostViewVM = SingleLineTableViewCellViewModel(text: "most_viewed_text".localizedCapitalized(), type: .mostViewed, accessoryType: .disclosureIndicator)
        mostViewVM.isLabelVerticallyCentered.accept(true)
        let mostSharedVM = SingleLineTableViewCellViewModel(text: "most_shared_text".localizedCapitalized(), type: .mostShared, accessoryType: .disclosureIndicator)
        mostSharedVM.isLabelVerticallyCentered.accept(true)
        let mostEmailedVM = SingleLineTableViewCellViewModel(text: "most_emailed_text".localizedCapitalized(), type: .mostEmailed, accessoryType: .disclosureIndicator)
        mostEmailedVM.isLabelVerticallyCentered.accept(true)
        popularCells.accept([
            mostViewVM,
            mostSharedVM,
            mostEmailedVM
        ])
        
    }
}
