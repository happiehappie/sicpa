//
//  HomeViewController.swift
//  NicolleTan
//
//  Created by Jack Xiong Lim on 17/7/21.
//

import UIKit
import RxSwift

class HomeViewController<ViewModel, Router>: UIViewController where ViewModel: HomeViewModelType, Router: HomeRoutable {
    
    var rootView: HomeView {
        return view as! HomeView
    }
    
    private(set) lazy var viewModel: ViewModel = ViewModel()
    private var router: Router
    private var disposeBag: DisposeBag!
    
    public init(viewModel: ViewModel, router: Router) {
        self.router = router
        
        super.init(nibName: nil, bundle: nil)
        
        self.viewModel = viewModel
    }
    
    public override func loadView() {
        view = HomeView(frame: UIScreen.main.bounds)
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AppRouterGeneral.sharedInstance().navigationController = navigationController
        setupView()
        setupListeners()
    }
    
    // MARK: - Private API -
    // MARK: Setup Methods
    
    private func setupView() {
        for section in HomeSection.allCases {
            rootView.tableView.registerCellClass(section.cellType)
        }
    }
    
    private func setupListeners() {
        disposeBag = DisposeBag()
        
        viewModel.sectionedItems
            .bind(to: rootView.tableView.rx.items(dataSource: viewModel.dataSource))
            .disposed(by: disposeBag)
        
        viewModel.title
            .subscribe(onNext: { [weak self] (value) in
                guard let strongSelf = self else { return }
                strongSelf.title = value
            })
            .disposed(by: disposeBag)
        
        viewModel.searchTitle
            .subscribe(onNext: { [weak self] (value) in
                guard let strongSelf = self else { return }
                strongSelf.viewModel.setSection(.searchTitle(item: value))
            })
            .disposed(by: disposeBag)
        
        viewModel.searchCell
            .subscribe(onNext: { [weak self] (value) in
                guard let strongSelf = self else { return }
                strongSelf.viewModel.setSection(.search(item: value))
            })
            .disposed(by: disposeBag)
        
        viewModel.popularTitle
            .subscribe(onNext: { [weak self] (value) in
                guard let strongSelf = self else { return }
                strongSelf.viewModel.setSection(.popularTitle(item: value))
            })
            .disposed(by: disposeBag)
        
        viewModel.popularCells
            .subscribe(onNext: { [weak self] (value) in
                guard let strongSelf = self else { return }
                strongSelf.viewModel.setSection(.popular(items: value))
            })
            .disposed(by: disposeBag)
        
        rootView.tableView.rx
            .itemSelected
            .subscribe(onNext: { [weak self] indexPath in
                guard let strongSelf = self, let cell = strongSelf.rootView.tableView.cellForRow(at: indexPath) as? SingleLineTableViewCell else { return }
                switch cell.viewModel.fetchType.value {
                case .search:
                    strongSelf.router.navigateToSearch()
                case .mostViewed, .mostShared, .mostEmailed:
                    guard let type = cell.viewModel.fetchType.value else { return }
                    strongSelf.router.navigateToArticleList(type: type)
                case .none:
                    break
                }
                
                
            }).disposed(by: disposeBag)
        
    }
}
