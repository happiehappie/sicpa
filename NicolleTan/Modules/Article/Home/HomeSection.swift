//
//  HomeSection.swift
//  NicolleTan
//
//  Created by Jack Xiong Lim on 19/7/21.
//

import Foundation
import RxDataSources

public enum HomeSection: SectionModelType {
    case searchTitle(item: SingleLineTableViewCellViewModel?)
    case search(item: SingleLineTableViewCellViewModel?)
    case popularTitle(item: SingleLineTableViewCellViewModel?)
    case popular(items: [SingleLineTableViewCellViewModel])
    
    public var items: [AnyObject] {
        switch self {
        case .searchTitle(let item), .popularTitle(let item), .search(let item):
            return item != nil ? [item!] : []
        case .popular(let items):
            return items
        }
    }
    
    public init(original: HomeSection, items: [AnyObject]) {
        switch original {
        case .searchTitle, .popularTitle, .search:
            self = .searchTitle(item: items.first as? SingleLineTableViewCellViewModel)
        case .popular:
            self = .popular(items: items as! [SingleLineTableViewCellViewModel])
        }
    }
}

extension HomeSection: TableViewDataSource {
    
    public typealias Section = HomeSection
    public static var allCases: [HomeSection] {
        return [
            .searchTitle(item: nil),
            .search(item: nil),
            .popularTitle(item: nil),
            .popular(items: [SingleLineTableViewCellViewModel]())
        ]
    }
    
    public static func generateDataSource() -> RxTableViewSectionedReloadDataSource<HomeSection> {
        return RxTableViewSectionedReloadDataSource<HomeSection>(configureCell: { (_, tableView, indexPath, viewModel) -> UITableViewCell in
            var cell: UITableViewCell!
            if let viewModel = viewModel as? SingleLineTableViewCellViewModel {
                let newCell = tableView.dequeueCell(SingleLineTableViewCell.self, at: indexPath)
                newCell.configureWith(value: viewModel)
                cell = newCell
            }
            
            return cell
        })
    }
    public var cellType: UITableViewCell.Type {
        switch self {
        case .searchTitle, .popularTitle, .search, .popular:
            return SingleLineTableViewCell.self
        }
    }
}

extension HomeSection: RelativeOrder {
    public var sectionOrder: Int {
        switch self {
        case .searchTitle:
            return 0
        case .search:
            return 1
        case .popularTitle:
            return 2
        case .popular:
            return 3
        }
    }
}
