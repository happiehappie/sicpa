//
//  HomeView.swift
//  NicolleTan
//
//  Created by Jack Xiong Lim on 19/7/21.
//

import Foundation
import UIKit

public class HomeView: UIView {
    lazy var tableView: UITableView = {
        let newTableView = UITableView()
        newTableView.translatesAutoresizingMaskIntoConstraints = false
        newTableView.estimatedRowHeight = 100
        newTableView.rowHeight = UITableView.automaticDimension
        newTableView.showsVerticalScrollIndicator = false
        return newTableView
    }()
    
    // MARK: - Initializer and Lifecycle Methods -
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        setupSubviews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupSubviews() {
        addSubview(tableView)
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
    
}
