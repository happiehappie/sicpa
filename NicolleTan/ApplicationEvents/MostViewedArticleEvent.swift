//
//  MostViewedArticleEvent.swift
//  NicolleTan
//
//  Created by Jack Xiong Lim on 22/7/21.
//

import Foundation

public class MostViewedArticleEvent: SerialBusEvent<MostPopularResponse> {
    public override class func saga() -> [SerialSubrequest] {
        return [SerialSubrequest(action: { params in
            return API.getMostViewedList(days: params.first as? Int ?? 1).map { $0 as Any }
        }, debugInfo: "GET /views/{period}.json")]
    }
}
