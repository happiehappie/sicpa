//
//  SearchArticleEvent.swift
//  NicolleTan
//
//  Created by Jack Xiong Lim on 21/7/21.
//

import Foundation
import RxSwift

public enum SearchArticleEventError: Error {
    case invalidParams
}

public class SearchArticleEvent: SerialBusEvent<ArticleResponse> {
    public override class func saga() -> [SerialSubrequest] {
        return [SerialSubrequest(action: { params in
            guard let query = params.first as? String else { return Single.error(SearchArticleEventError.invalidParams) }
            return API.getCampaignList(query: query).map { $0 as Any }
        }, debugInfo: "GET /articlesearch.json?q={query}")]
    }
}
