//
//  MostSharedArticleEvent.swift
//  NicolleTan
//
//  Created by Jack Xiong Lim on 22/7/21.
//

import Foundation

public class MostSharedArticleEvent: SerialBusEvent<MostPopularResponse> {
    public override class func saga() -> [SerialSubrequest] {
        return [SerialSubrequest(action: { params in
            return API.getMostSharedList(days: params.first as? Int ?? 1).map { $0 as Any }
        }, debugInfo: "GET /shared/{period}.json")]
    }
}
